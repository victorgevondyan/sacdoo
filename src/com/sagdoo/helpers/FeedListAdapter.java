package com.sagdoo.helpers;

import java.util.ArrayList;

import android.app.ProgressDialog;
import android.app.Service;
import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.sagdoo.Feed;
import com.sagdoo.R;
import com.sagdoo.models.Comment;
import com.sagdoo.models.User;
import com.sagdoo.talkers.APITalker;
import com.sagdoo.talkers.ImageLoader;
import com.sagdoo.talkers.LikeHandler;

public class FeedListAdapter extends ArrayAdapter<Comment> implements LikeHandler{
	private Comment likedComment;
	private int resource;
	private LayoutInflater inflater;
	private ArrayList<Comment> comments;
	private ArrayList<String> data;
	public ImageLoader imageLoader;
	public Context context;
	public ProgressDialog loadingDialog;
	
	public FeedListAdapter(Context context, int resource, ArrayList<Comment> comments, ArrayList<String> data ) {
		super(context, resource, comments);
		
		this.context = context;
		this.comments = comments;
		this.inflater = (LayoutInflater) context.getSystemService(Service.LAYOUT_INFLATER_SERVICE);
		this.resource = resource;
//		Log.d("CONSTRUCTOR DATA", data.toString());
		this.data = data;
//		Log.d( "FEED LIST DATA FIELD", (this.data).toString() );
		imageLoader = new ImageLoader(context);
	}
	
	@Override
	
	public View getView(int position, View convertView, ViewGroup parent) {
		
		if (convertView == null) {
			convertView = inflater.inflate(resource, parent, false);
			Typeface fontawesome = Typeface.createFromAsset( context.getAssets(), "fontawesome.ttf");
			TextView urlIcon = (TextView) convertView.findViewById(R.id.url_icon);
			TextView commentIcon = (TextView) convertView.findViewById(R.id.comment_icon);
			TextView likeIcon = (TextView) convertView.findViewById(R.id.like_icon);
			
			urlIcon.setTypeface(fontawesome);
			commentIcon.setTypeface(fontawesome);
			likeIcon.setTypeface(fontawesome);
			
			
			final ListView listView = ( ListView )parent;
			
			View.OnClickListener LikeClickListener = new View.OnClickListener() {
				
				@Override
				public void onClick( View view ) {
					String token = User.sharedUser( context ).token();
					
					likedComment = getCommentObject( view, listView );
					String commentId = likedComment.getId();
					Log.d("COMMENT ID", commentId);
					APITalker.sharedTalker().like( token, commentId, FeedListAdapter.this );
				}
			};
			
			likeIcon.setOnClickListener( LikeClickListener );
		}
		
		
		
		TextView url = (TextView) convertView.findViewById(R.id.url);
		TextView comment = (TextView) convertView.findViewById(R.id.comment);
		TextView user = (TextView) convertView.findViewById(R.id.user);
		TextView tags = (TextView) convertView.findViewById(R.id.tags_description);
		TextView likesCount = (TextView) convertView.findViewById(R.id.likes_count);
		ImageView picture = (ImageView) convertView.findViewById(R.id.user_picture);
		
		Comment commentObject = getItem(position);
		comment.setText(commentObject.getDescription());

		url.setText(commentObject.getUrl());
		user.setText(commentObject.getFriend().firstName() + " " + commentObject.getFriend().lastName());
		tags.setText(commentObject.getTags());
		likesCount.setText( Integer.toString( commentObject.getLikesCount() ) );
		
		String urlString = commentObject.getFriend().imageUrl();
//		Log.d("URL STRING", data.get(position) );
		if ( urlString != "" ){
			imageLoader.DisplayImage( urlString, picture );
		}
		
		TextView time = (TextView) convertView.findViewById(R.id.time);

		if (commentObject.getDate() != null) {
			time.setText(DateUtils.getRelativeDateTimeString( context,
					commentObject.getDate().getTime(),
					DateUtils.SECOND_IN_MILLIS,
					DateUtils.YEAR_IN_MILLIS,
					DateUtils.FORMAT_ABBREV_RELATIVE).toString());
		}
		
		return convertView;
	}
	
	

	@Override
	public Comment getItem(int position) {
		return comments.get(position);
	}
	
	public void addAllAtBeginning( ArrayList<Comment> newComments ) {
		Log.d("NEW COMMENTS", newComments.toString());
		newComments.addAll(comments);
		comments = newComments;
		notifyDataSetChanged();
	}
	
	public void addAllAtEnd(ArrayList<Comment> newComments) {
		comments.addAll(newComments);
		notifyDataSetChanged();
	}
	
	@Override
	public int getCount() {
		return comments.size();
	}
	
	public void checkAndAdd(Comment comment) {
		for (Comment oldComment : comments) {
			if (comment.getId().equals(oldComment.getId())) {
				return;
			}
		}
		
		comments.set(0, comment);
		notifyDataSetChanged();
	}
	
	
	public void onLike( View view ){
		
	}
	
	//GETS THE COMMENT OBJECT BY THE VIEW OF LIST ITEM , IN WHICH THIS COMMENT DISPLAYED
		public Comment getCommentObject( View view , ListView listView){
			View parentView = (View)view.getParent();
			int position = listView.getPositionForView(parentView);
			Comment viewRealParent = getItem(position);
			return viewRealParent;			
		}
		
		@Override
		public void likeSucceed( int likesCount ) {
			likedComment.setLikesCount( likesCount );
			notifyDataSetChanged();
		}

		@Override
		public void likeFailed(String error) {
			Helper.showToast(error, context, loadingDialog);
		}
	


	
	
}
