package com.sagdoo.helpers;

import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.ListView;

import com.sagdoo.models.Comment;
import com.sagdoo.models.User;
import com.sagdoo.talkers.APITalker;
import com.sagdoo.talkers.LikeHandler;

public class LikeHelper {
	public ListView listView;
	public FeedListAdapter adapter;
	public Context context;
	
	public Comment likedComment;
	public ProgressDialog loadingDialog;
	
	
	
	public LikeHelper( Context context, ListView listView, FeedListAdapter adapter ){
		this.context = context;
		this.listView = listView;
		this.adapter = adapter;
		( this.listView ).setAdapter( this.adapter );
	}
	
	public void setMyAdapter( ListView listView, FeedListAdapter adapter ){
		listView.setAdapter(adapter);
	} 
	
	

}
