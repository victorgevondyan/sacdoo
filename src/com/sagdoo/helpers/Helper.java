package com.sagdoo.helpers;

import android.app.ProgressDialog;
import android.content.Context;
import android.widget.Toast;

public class Helper {
	public static void showToast(String text, Context context,
			ProgressDialog loadingDialog) {
		int duration = Toast.LENGTH_SHORT;
		Toast toast = Toast.makeText(context, text, duration);
		toast.show();
		if( loadingDialog != null ){
			loadingDialog.dismiss();
		}
	}
}
