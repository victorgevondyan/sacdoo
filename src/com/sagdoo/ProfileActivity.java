package com.sagdoo;

import java.io.File;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.sagdoo.helpers.Helper;
import com.sagdoo.models.User;
import com.sagdoo.talkers.APITalker;
import com.sagdoo.talkers.GetImageHandler;
import com.sagdoo.talkers.UploadImageHandler;
import com.sagdoo.talkers.UserHandler;

public class ProfileActivity extends Activity implements UserHandler,
		UploadImageHandler, GetImageHandler {
	private final String SELECTED_IMAGE_PATH = "selectedImagePath";
	private EditText firstNameEditText;
	private EditText lastNameEditText;
	private EditText userNameEditText;
	private EditText emailEditText;
	private EditText mobileEditText;
	private ImageView profileImageView;
	private Bitmap profileImage;

	private String id;
	private String selectedImagePath;

	private static ProgressDialog loadingDialog;

	// This is the action code we use in our intent,
	// this way we know we're looking at the response from our own action
	private static final int SELECT_PICTURE = 1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_profile);

		loadingDialog = new ProgressDialog(this);

		Intent intent = getIntent();
		id = intent.getStringExtra("USER_ID");
		APITalker.sharedTalker().getUserById(id, this);

		firstNameEditText = (EditText) findViewById(R.id.fname);
		lastNameEditText = (EditText) findViewById(R.id.lname);
		userNameEditText = (EditText) findViewById(R.id.uname);
		emailEditText = (EditText) findViewById(R.id.email);
		mobileEditText = (EditText) findViewById(R.id.mobile);
		profileImageView = (ImageView) findViewById(R.id.profile_imageView);
		Button updateProfileButton = (Button) findViewById(R.id.update);
		updateProfileButton.setOnClickListener(updateProfileListener);

		// A button to perform profile image upload
		Button addImageButton = (Button) findViewById(R.id.add_image);
		addImageButton.setOnClickListener(addImageListener);

		if (savedInstanceState != null) {
			String savedSelectedImagePath = savedInstanceState
					.getString(SELECTED_IMAGE_PATH);
			profileImage = BitmapFactory.decodeFile(savedSelectedImagePath);
			profileImageView.setImageBitmap(profileImage);
		}
	}

	protected void onSaveInstanceState(Bundle outState) {
		outState.putString(SELECTED_IMAGE_PATH, selectedImagePath);
	};

	OnClickListener updateProfileListener = new OnClickListener() {

		@Override
		public void onClick(View v) {

			final String firstName = firstNameEditText.getText().toString();
			final String lastName = lastNameEditText.getText().toString();
			final String userName = userNameEditText.getText().toString();
			final String email = emailEditText.getText().toString();
			final String mobile = mobileEditText.getText().toString();

			if (firstName.equals("") || lastName.equals("")
					|| userName.equals("") || email.equals("")
					|| mobile.equals("")) {
				Toast toast = Toast.makeText(getApplicationContext(),
						"Please fill all the information", Toast.LENGTH_SHORT);
				toast.show();
			} else {
				loadingDialog.setCancelable(false);
				loadingDialog.setCanceledOnTouchOutside(false);
				loadingDialog.show();
				APITalker.sharedTalker().updateProfile(firstName, lastName,
						userName, email, mobile, id, ProfileActivity.this);
			}

		}
	};

	// A listener to handle Add Image button click
	OnClickListener addImageListener = new OnClickListener() {


		@Override
		public void onClick(View v) {

			// in onCreate or any event where your want the user to
			// select a file
			Intent intent = new Intent();
			intent.setType("image/*");
			intent.setAction(Intent.ACTION_GET_CONTENT);
			startActivityForResult(
					Intent.createChooser(intent, "Select Picture"),
					SELECT_PICTURE);

		}

	};

	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == RESULT_OK) {
			if (requestCode == SELECT_PICTURE) {
				Uri selectedImageUri = data.getData();
				selectedImagePath = getPath(selectedImageUri);
				
				File Img = new File(selectedImagePath);
				long length = Img.length();
				
				if( length > 1048576 ){
					
					int duration = Toast.LENGTH_SHORT;
					Toast toast = Toast
							.makeText(
									this,
									"THE IMAGE IS TOO BIG. PLEASE CHOOSE ANOTHER IMAGE",
									duration);
					toast.show();
					return;
				}
				
				profileImage = BitmapFactory.decodeFile(selectedImagePath);
				loadingDialog.setCancelable(false);
				loadingDialog.setCanceledOnTouchOutside(false);
				loadingDialog.show();
				APITalker.sharedTalker().uploadImage(
						User.sharedUser(this).token(), selectedImagePath,
						ProfileActivity.this);

			}
		}

	}

	/**
	 * helper to retrieve the path of an image URI
	 */
	public String getPath(Uri uri) {
		// just some safety built in
		if (uri == null) {
			// TODO perform some logging or show user feedback
			return null;
		}
		// try to retrieve the image from the media store first
		// this will only work for images selected from gallery
		String[] projection = { MediaStore.Images.Media.DATA };
		Cursor cursor = getContentResolver().query(uri, projection, null, null,
				null);
		if (cursor != null) {
			int column_index = cursor
					.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
			cursor.moveToFirst();
			String path = cursor.getString(column_index);
			cursor.close();
			return path;
		}
		// this is our fallback here
		return uri.getPath();
	}

	@Override
	public void getUserSucceed(String firstName, String lastName,
			String userName, String email, String mobile, String profileImageUri) {
		firstNameEditText.setText(firstName);
		lastNameEditText.setText(lastName);
		userNameEditText.setText(userName);
		emailEditText.setText(email);
		mobileEditText.setText(mobile);
		APITalker.sharedTalker().getImage(this, User.sharedUser(this).token(),
				profileImageUri, ProfileActivity.this);

		/*
		 * if( profileImageUri != "" ){ String noSlashesUri =
		 * profileImageUri.replace("\\" , " ").replace(".", ""); profileImage =
		 * BitmapFactory.decodeFile( noSlashesUri );
		 * profileImageView.setImageBitmap( profileImage ); }
		 */
		loadingDialog.dismiss();
	}

	@Override
	public void getUserFailed(String error) {
		Helper.showToast( error, this, loadingDialog );
	}

	@Override
	public void uploadImageSucceed(String profileImageUri) {
		APITalker.sharedTalker().getImage(this, User.sharedUser(this).token(),
				profileImageUri, ProfileActivity.this);
	}

	@Override
	public void uploadImageFailed(String error) {
		Helper.showToast( error, this, loadingDialog );
	}

	@Override
	public void getImageSucceed(String profileImageUri) {
		if ( profileImageUri != "") {
			profileImage = BitmapFactory.decodeFile( profileImageUri );
			profileImageView.setImageBitmap(profileImage);
			loadingDialog.dismiss();
		}
	}

	@Override
	public void getImageFailed(String error) {
		Helper.showToast( error, this, loadingDialog );
	}

	
}
