package com.sagdoo;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.Toast;

import com.sagdoo.models.User;
import com.sagdoo.talkers.APITalker;
import com.sagdoo.talkers.RegisterHandler;

public class Register extends Activity implements RegisterHandler {
	public static final int REQUEST_CODE = 897645;

	private boolean validInputs = false;

	private EditText txtfname;
	private EditText txtlname;
	private EditText txtuname;
	private EditText txtpwd;
	private EditText txtemail;
	private EditText txtmobile;

	ProgressDialog loadingDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_register);
		
		loadingDialog = new ProgressDialog(this);
		
		txtfname = (EditText) findViewById(R.id.fname);
		txtlname = (EditText) findViewById(R.id.lname);
		txtuname = (EditText) findViewById(R.id.uname);
		txtpwd = (EditText) findViewById(R.id.pwd);
		txtemail = (EditText) findViewById(R.id.email);
		txtmobile = (EditText) findViewById(R.id.mobile);
		
		findViewById(R.id.register).setOnClickListener(new OnClickListener() {
			
			public boolean isLessThanThree(String string){
				if(string.length()<3){
					return true;
				}
				else{
					return false;
				}
			}
			
			public boolean isValidEmail( String email ){
				return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
			}
			
			public boolean isValidPhone( String email ){
				return android.util.Patterns.PHONE.matcher(email).matches();
			}
			
			public void showErrorToast( String string ){
				Toast toast = Toast.makeText(getApplicationContext(),
						string,
						Toast.LENGTH_SHORT);
				toast.show();
			}
			
			
			public void validateInputs(final String fname, final String lname,
					final String uname, final String pwd, final String email,
					final String mobile) {
				
				if(fname.equals("") || lname.equals("") || uname.equals("") || pwd.equals("") || email.equals("") || mobile.equals("")){
					showErrorToast("Please fill all the information");
				} else if( isLessThanThree(fname) ){
					showErrorToast("First Name must contain at least 3 letters");
				}else if( isLessThanThree(lname)){
					showErrorToast("Last Name must contain at least 3 letters");
				}else if( isLessThanThree(uname) ){
					showErrorToast("User Name must contain at least 3 letters");
				}else if( isLessThanThree(pwd) ){
					showErrorToast("Password must contain at least 3 letters");
				}else if( isLessThanThree(uname) ){
					showErrorToast("User Name must contain at least 3 letters");
				}else if( !isValidEmail(email)){
					showErrorToast("Not valid Email");
				}else if( !isValidPhone(mobile)){
					showErrorToast("Not valid Mobile number");
				}else{
					validInputs = true;
				}
				
			}
			
			@Override
			public void onClick(View view) {
				final String fname = txtfname.getText().toString();
				final String lname = txtlname.getText().toString();
				final String uname = txtuname.getText().toString();
				final String pwd = txtpwd.getText().toString();
				final String email = txtemail.getText().toString();
				final String mobile = txtmobile.getText().toString();
				
				if(!validInputs){
					validateInputs(fname, lname, uname, pwd, email, mobile);
				}
				
				else {
					loadingDialog.setCancelable(false);
					loadingDialog.setCanceledOnTouchOutside(false);
					loadingDialog.show();
					APITalker.sharedTalker().register(uname, pwd, email, mobile, fname, lname, Register.this);
				}
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onActivityResult(int request, int response, Intent intent) {
		if (request == Browser.ACTIVITY_RESULT) {
			setResult(RESULT_OK);
			finish();
		}
	}

	/*
	 * RegisterHandler Methods
	 */

	@Override
	public void registerSucceed(String username, String password,
			String userId, String token, String firstName, String lastName, String profileImageUri ) {
		loadingDialog.dismiss();
		User.sharedUser(this).login( username, password, userId, token,
				firstName, lastName, profileImageUri );

		Intent intent = new Intent(Register.this, Browser.class);
		startActivityForResult(intent, Browser.ACTIVITY_RESULT);
	}

	@Override
	public void registerFailed(String error) {
		loadingDialog.dismiss();
		Toast toast = Toast.makeText(getApplicationContext(), error,
				Toast.LENGTH_SHORT);
		toast.show();
	}
}