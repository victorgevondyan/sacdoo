package com.sagdoo;

import java.util.ArrayList;

import android.app.Activity;
import android.app.ProgressDialog;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.sagdoo.models.Friend;
import com.sagdoo.models.User;
import com.sagdoo.talkers.APITalker;
import com.sagdoo.talkers.FriendsHandler;
import com.sagdoo.talkers.NewFriendsHandler;
import com.sagdoo.talkers.UsersHandler;

public class FriendRequestActivity extends Activity implements UsersHandler,FriendsHandler,
		NewFriendsHandler, OnClickListener {

	AddFriendsAdapter adapter;
	
	ProgressDialog loadingDialog;
	
	private boolean lockRequests = false;
	private boolean lockForever = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_friend_request);
		
		loadingDialog = new ProgressDialog(this);
		loadingDialog.setCancelable(false);
		loadingDialog.setCanceledOnTouchOutside(false);
		loadingDialog.show();
		lockRequests = true;
		
		Intent intent = getIntent();
		String intentRoot = intent.getStringExtra(Intent.EXTRA_TEXT);
		String action = intent.getAction();
		if( action == "FEED" )
		{
			APITalker.sharedTalker().getFriends(
					User.sharedUser(FriendRequestActivity.this).userId(), false, 25, 0, 0,
					FriendRequestActivity.this);
		}
		
		else if( action == "NEW_FRIENDS" )
		{
			APITalker.sharedTalker().getUsers( 25, 0 ,this);
		}
		
		adapter = new AddFriendsAdapter(this,R.layout.add_friend_item,new ArrayList<Friend>());
		ListView listView = (ListView) findViewById(R.id.freinds);
		listView.setAdapter(adapter);
		
		listView.setOnScrollListener(NewFriendsListScrollListener);
	}
	
	public OnScrollListener NewFriendsListScrollListener = new OnScrollListener() {

		@Override
		public void onScrollStateChanged(AbsListView view, int scrollState) {
			
		}

		@Override
		public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
			if ( firstVisibleItem >= totalItemCount - 10 && !lockForever && !lockRequests ) {
				lockRequests = true;
				APITalker talker = APITalker.sharedTalker();
				
				if (FriendRequestActivity.this.getIntent().getAction() == "FEED") {
					talker.getFriends(
							User.sharedUser(FriendRequestActivity.this).userId(), false, 25, adapter.getCount(), 0,
							FriendRequestActivity.this);
				} else {
					talker.getUsers( 25, adapter.getCount() , FriendRequestActivity.this );
				}
			}
		}
	};

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		return super.onOptionsItemSelected(item);
	}

	/*
	 * ListView Adapter
	 */

	private class AddFriendsAdapter extends ArrayAdapter<Friend> {
		private Context context;
		private LayoutInflater inflater;
		private ArrayList<Friend> friends;
		private int resource;

		public AddFriendsAdapter(Context context, int resource,
				ArrayList<Friend> friends) {
			super(context, resource, friends);

			this.context = context;
			this.resource = resource;
			this.friends = friends;
			this.inflater = (LayoutInflater) context
					.getSystemService(Service.LAYOUT_INFLATER_SERVICE);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			if (convertView == null) {
				convertView = inflater.inflate(resource, parent, false);

				TextView addIcon = (TextView) convertView
						.findViewById(R.id.add_icon);
				addIcon.setTypeface(Typeface.createFromAsset(
						context.getAssets(), "fontawesome.ttf"));
				addIcon.setOnClickListener(FriendRequestActivity.this);
			}

			Friend friend = getItem(position);
			TextView name = (TextView) convertView.findViewById(R.id.name);
			name.setText(friend.firstName() + " " + friend.lastName());

			return convertView;
		}

		@Override
		public Friend getItem(int position) {
			return friends.get(position);
		}
		
		public void addAtEnd(ArrayList<Friend> newFriends) {
			friends.addAll(newFriends);
			notifyDataSetChanged();
		}
	}

	/*
	 * OnClickListener Methods
	 */

	@Override
	public void onClick(View view) {
		if (view.getId() == R.id.add_icon) {
			
			View parent = (View) view.getParent();
			ListView listView = (ListView) findViewById(R.id.freinds);
			int index = listView.getPositionForView(parent);
			Friend newFriend = adapter.getItem(index);
			String id = newFriend.id();	
			loadingDialog.setCancelable(false);
			loadingDialog.setCanceledOnTouchOutside(false);
			loadingDialog.show();
			
			APITalker.sharedTalker().addFriend(User.sharedUser(this).token(),
					id, this);
			Log.d("INDEX", index + "");
			
		}
	}

	/*
	 * UsersHandler Methods
	 */

	@Override
	public void getUsersSucceed(ArrayList<Friend> friends, int requestId) {
		loadingDialog.dismiss();
		
		if (friends.size() > 0) {
			adapter.addAtEnd(friends);
		}
		
		if (friends.size() < 25) {
			lockForever = true;
		}
		
		lockRequests = false;
	}

	@Override
	public void getUsersFailed(String error, int requestId) {
		Context context = getApplicationContext();
		CharSequence text = error;
		int duration = Toast.LENGTH_SHORT;

		Toast toast = Toast.makeText(context, text, duration);
		toast.show();
		
		loadingDialog.dismiss();
		
		
		lockRequests = false;
	}

	/*
	 * NewFriendsHandler Methods
	 */

	@Override
	public void addNewFriendsSucceed() {
		Context context = getApplicationContext();
		CharSequence text = "Add New Friend Succeed";
		int duration = Toast.LENGTH_SHORT;

		Toast toast = Toast.makeText(context, text, duration);
		toast.show();
		
		loadingDialog.dismiss();
	}

	@Override
	public void addNewFriendsFailed(String error) {
		Context context = getApplicationContext();
		CharSequence text = error;
		int duration = Toast.LENGTH_SHORT;

		Toast toast = Toast.makeText(context, text, duration);
		toast.show();
		
		loadingDialog.dismiss();
	}

	@Override
	public void getFriendsSuccess(ArrayList<Friend> friends) {
		TextView noFriendsTextView = (TextView) findViewById(R.id.no_friends_add);
		if( friends.isEmpty() ) {
			noFriendsTextView.setVisibility(View.VISIBLE);
		}
		
		loadingDialog.dismiss();
		
		if (friends.size() > 0) {
			adapter.addAtEnd(friends);
		}
		
		if (friends.size() < 25) {
			lockForever = true;
		}
		
		lockRequests = false;
		
	}

	@Override
	public void getFriendsFailure(String error) {
		loadingDialog.dismiss();
		
		Context context = getApplicationContext();
		CharSequence text = error;
		int duration = Toast.LENGTH_SHORT;

		Toast toast = Toast.makeText(context, text, duration);
		toast.show();
		
		lockRequests = false;
		
	}
	
	public void onBack(View view){
		
		finish();
	}
}


