
package com.sagdoo;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Typeface;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.sagdoo.gcm.GcmRegistrerHelper;
import com.sagdoo.gcm.GcmUtils;
import com.sagdoo.helpers.FeedListAdapter;
import com.sagdoo.helpers.Helper;
import com.sagdoo.helpers.LikeHelper;
import com.sagdoo.models.Comment;
import com.sagdoo.models.Friend;
import com.sagdoo.models.User;
import com.sagdoo.talkers.APITalker;
import com.sagdoo.talkers.CommentHandler;
import com.sagdoo.talkers.FeedHandler;

public class Feed extends Activity implements FeedHandler, CommentHandler,
		LocationListener{
	public static int ACTIVITY_RESULT = 9900;
	public static final String INTENT_ROOT = "intentRoot";
	public static final String SAVED_URL = "Feed.savedUrl";
	public static final String ORIGINAL_SAVED_URL = "Feed.originalSavedUrl";
	public static final String COMMENT_MESSAGE = "Feed.commentMessage";
	public static final String COMMENT_BOX_OPENED = "Feed.commentBoxOpened";
	public static final String COMMENT_TAGS = "commentTags";
	public static final String COMMENT_LIKES = "commentLikes";
	public static final String COMMENT_URL = "commentUrl";
	public static final String COMMENTER_FRIENDS = "commenterFriends";
	public static final String ACTION_BROWSER = "actionBrowser";
	
	//public static final String CLICKED_URL = "clickedUrl";
	
	ArrayList<Friend> returnedCheckedFriendsArray = new ArrayList<Friend>();
	
	ArrayList<Comment> comments = new ArrayList<Comment>();
	
	private LocationManager locationManager;
	
	private Location location;
	
	private RelativeLayout commentBox;
	private TextView addFriendsButton;
	private TextView sendMessageButton;
	private TextView friendsTagged;
	private TextView cancelButton;
	private EditText commentMessageEditText;
	private EditText commentTagsEditText;
	private String commentTagsString;
	private Comment likedComment;
	private static String lastClickedCommentUrl;   /*THE URL CORRESPONDING TO THE VIEW , IN WHICH THE LAST 
	
	"Comment" BUTTON WAS CLICKED*/ 
	private static ProgressDialog loadingDialog;
	private LikeHelper likeHelper;
	private FeedListAdapter adapter;
	private ListView feed;
	
	private boolean lockRequest = false;
	private boolean lockForever = false;
	private boolean movedRight = false;
	private boolean opened = false;

	private ArrayList<String> imageUrls = new ArrayList<String>();
	
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_feed);
		
		loadingDialog = new ProgressDialog(this);
		loadingDialog.setCancelable(false);
		loadingDialog.setCanceledOnTouchOutside(false);
		loadingDialog.show();
		
		lockRequest = true;
		
		adapter = new FeedListAdapter(this, R.layout.feed_comment_item, new ArrayList<Comment>(), imageUrls );
		feed = (ListView) findViewById(R.id.feed_list);
		feed.setAdapter(adapter);
		feed.setOnScrollListener(feedListScrollListener);
		
		APITalker talker = APITalker.sharedTalker();
		talker.getComments(User.sharedUser(this).token(), null, 25 , 0 , this);
		
//		INITIALIZE A COMMENT BOX 	
		commentBox = (RelativeLayout) findViewById(R.id.comment_box);
		Typeface fontawesome = Typeface.createFromAsset(getAssets(), "fontawesome.ttf");
		
		// A box for writing comments(messages) 
		commentMessageEditText = (EditText) findViewById(R.id.message);
		commentTagsEditText = (EditText)findViewById(R.id.comment_tag);
		addFriendsButton = (TextView) findViewById(R.id.add_friends);
		sendMessageButton = (TextView) findViewById(R.id.send_message);
		friendsTagged = (TextView) findViewById(R.id.tagged_friends);
		friendsTagged.setText("Tagged friends: none");
		cancelButton = (TextView) findViewById(R.id.cancel);
		
		cancelButton.setOnClickListener(closeCommentBoxClickListener);
		
		addFriendsButton.setTypeface(fontawesome);
		sendMessageButton.setTypeface(fontawesome);
		cancelButton.setTypeface(fontawesome);
		
		Button searchButton = (Button) findViewById(R.id.button_search);
		Button friendRequestButton = (Button) findViewById(R.id.button_friend_request);
		Button followersButton = (Button) findViewById(R.id.button_followers);
		Button friendsButton = (Button) findViewById(R.id.button_friends);
		Button profileButton = (Button) findViewById(R.id.button_profile);
		Button logoutButton = (Button) findViewById(R.id.button_logout);
		Button circlesButton = (Button) findViewById(R.id.button_circle);
		Button browseButton = (Button) findViewById(R.id.button_browse);
		Button menuButton = (Button) findViewById(R.id.button_menu);
		
		searchButton.setOnClickListener(buttonsClickListener);
		friendsButton.setOnClickListener(buttonsClickListener);
		logoutButton.setOnClickListener(buttonsClickListener);
		friendRequestButton.setOnClickListener(buttonsClickListener);
		profileButton.setOnClickListener(buttonsClickListener);
		circlesButton.setOnClickListener(buttonsClickListener);
		browseButton.setOnClickListener(buttonsClickListener);
		followersButton.setOnClickListener(buttonsClickListener);
		menuButton.setOnClickListener(buttonsClickListener);
		
		
		
		Typeface font = Typeface.createFromAsset( getAssets(), "fontawesome.ttf" );
		menuButton.setTypeface(font);
		
		try {
			GcmRegistrerHelper.registerForGcm(User.sharedUser(this).userId(), this);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		IntentFilter filter = new IntentFilter("FEED");
		registerReceiver(gcmReciever, filter);
		
		if ( savedInstanceState != null ){

			opened = savedInstanceState.getBoolean(COMMENT_BOX_OPENED);
			
			String obtainedCommentMessage = savedInstanceState.getString(COMMENT_MESSAGE);
			commentMessageEditText.setText(obtainedCommentMessage);
			
			String obtainedCommentTags = savedInstanceState.getString(COMMENT_TAGS);
			commentTagsEditText.setText(obtainedCommentTags);

		}
		
		try {
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            boolean isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            boolean isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

            if (isGPSEnabled == false && isNetworkEnabled == false) {
            	Log.d("NO PROVIDER", "PICHALKA");
            } else {
            	if (isGPSEnabled) {
                    locationManager.requestLocationUpdates(
                            LocationManager.GPS_PROVIDER,
                            Browser.MIN_TIME_BW_UPDATES,
                            Browser.MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                }
                if (isNetworkEnabled) {
                    locationManager.requestLocationUpdates(
                            LocationManager.NETWORK_PROVIDER,
                            Browser.MIN_TIME_BW_UPDATES,
                            Browser.MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                }
            }

        } catch (Exception e) {
        	Log.d("SHIT HAPPENS", "GEO");
            e.printStackTrace();
        }
		
//		boolean showAd = getSharedPreferences("SAGDOOPREFS", 0).getBoolean("SHOWAD", true);
//		if (showAd) {
//			AlertDialog.Builder dialog = new AlertDialog.Builder(getApplicationContext());
//			dialog.setTitle("Check out Sagdoo widget!");
//			dialog.setMessage("Check out Sagdoo widget! Get instantly to our browser to comment on new interesting entries!");
//			dialog.show();
//		}
	}
	
	@Override
	protected void onSaveInstanceState(Bundle savedInstanceState) {
		
		savedInstanceState.putBoolean(COMMENT_BOX_OPENED, opened);
		
		commentMessageEditText = (EditText) findViewById(R.id.message);
		String commentMesage = commentMessageEditText.getText().toString();
		savedInstanceState.putString(COMMENT_MESSAGE, commentMesage);
		
		savedInstanceState.putString(COMMENT_TAGS, commentTagsString);
		
		super.onSaveInstanceState(savedInstanceState);
	}
	
	/*
	 * Commenting Method
	 */
	
	public void sendMessage(View view) {
		EditText messageText = (EditText) findViewById(R.id.message);
		
		String message = messageText.getText().toString();
		commentTagsString = commentTagsEditText.getText().toString();
		if (message.length() < 1 || lastClickedCommentUrl == null || lastClickedCommentUrl.length() == 0) {
			Toast.makeText(this, "No comment or no url link", Toast.LENGTH_SHORT).show();
			return;
		}
		
		
		messageText.setText("");
		loadingDialog.setCancelable(false);
		loadingDialog.setCanceledOnTouchOutside(false);
		loadingDialog.show();
		
		
		int length = returnedCheckedFriendsArray.size();
		
		String[] returnedFriendsIndexesArray = new String[length];
		int i;
		for( i=0; i < length; i++ ){
			returnedFriendsIndexesArray[i] = returnedCheckedFriendsArray.get(i).id();
		}
		
//		new AsyncTask<Void, Void, Void>() {
//
//			@Override
//			protected Void doInBackground(Void... params) {
//				GcmUtils.sendMessage("URL_MY", "MESSAGE_MY");
//				return null;
//			}
//			
//		}.execute(null, null, null);
		
		APITalker.sharedTalker().addComment(
				User.sharedUser(this).token(),
				lastClickedCommentUrl,
				location == null ? -1 : location.getLongitude(),
				location == null ? -1 : location.getLatitude(),
				message,
				returnedFriendsIndexesArray,
				commentTagsString,
				this);
	}
	
	/*
	 * Adding followers methods
	 */
	
	public void addFriends(View view) {
		Intent friendsIntent = new Intent(this, AddFriendsActivity.class);
		startActivityForResult(friendsIntent, Browser.ADD_FRIENDS_REQUEST_CODE);
		
	}
	
	/*
	 * (non-Javadoc)
	 * @see android.app.Activity#onActivityResult(int, int, android.content.Intent)
	 * Also adds comments
	 */
	
	
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == Browser.ADD_FRIENDS_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
			returnedCheckedFriendsArray = data
					.getParcelableArrayListExtra(AddFriendsActivity.CHECKED_FRIENDS_LIST);
			String tagged = "Tagged friends: ";
			
			for (int index = 0 ; index < returnedCheckedFriendsArray.size() ; index++) {
				tagged = tagged + returnedCheckedFriendsArray.get(index).firstName() + " " + returnedCheckedFriendsArray.get(index).lastName();
				
				if (index != returnedCheckedFriendsArray.size() - 1) {
					tagged = ", ";
				}
			}
			
			if (returnedCheckedFriendsArray.size() == 0) {
				tagged = tagged + "none";
			}
			
			friendsTagged.setText(tagged);
		} else if (requestCode == Browser.ACTIVITY_RESULT && resultCode == Activity.RESULT_OK) {
			ArrayList<Comment> receivedComments =  data.getParcelableArrayListExtra(Browser.COMMENTS_LIST);
			Log.d("RECEIVED COMMENTS", receivedComments.toString());
			if ( receivedComments != null ) {
				adapter.addAllAtBeginning(receivedComments);
			}
		}
	}
	
	// CLOSE A COMMENT BOX WITH ANIMATION
	
	public void closeCommentBox(){
		
	cancelButton.animate().alpha(0);
	commentBox.animate().x(commentBox.getWidth());
	
	}
	
	OnClickListener closeCommentBoxClickListener = new OnClickListener() {
		
		@Override
		public void onClick(View view) {
			
			closeCommentBox();
		}
	};
	
	
	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		RelativeLayout underlyingRelativeLayout = (RelativeLayout) 
				findViewById(R.id.underlying_relative_layout);
		Context context = getApplicationContext();
		Log.d("WIDTH", "" + underlyingRelativeLayout.getLayoutParams().width);
		
		if (!movedRight) {
			underlyingRelativeLayout.setX(-underlyingRelativeLayout.getMeasuredWidth());
			underlyingRelativeLayout.setVisibility(View.VISIBLE);
			underlyingRelativeLayout.setY(findViewById(R.id.sagdoo_logo_image_view).getMeasuredHeight());
		}
	}
	
	@Override
	public void onDestroy() {
		unregisterReceiver(gcmReciever);
		
		super.onDestroy();
	}

	public OnScrollListener feedListScrollListener = new OnScrollListener() {



		@Override
		public void onScrollStateChanged( AbsListView view, int scrollState ) {
			
		}

		@Override
		public void onScroll(AbsListView view, int firstVisibleItem,
				int visibleItemCount, int totalItemCount) {
			
			if ( firstVisibleItem >= totalItemCount - 10 && !lockRequest && !lockForever )
			{
				Log.d( "ENTERING LISTS??", "YEH" );
				APITalker talker = APITalker.sharedTalker();
				Context context = getApplicationContext();
				Log.d("SKIPING", adapter.getCount() + "");
				talker.getComments( User.sharedUser(context).token(), null, 25, adapter.getCount(), Feed.this);
			}
		}
	};
	
	/*Sets background and text color for the button*/
	
	public void setColor( Button button, int backgroundColor, int textColor )
	{
		button.setBackgroundColor(backgroundColor);
		button.setTextColor(textColor);
	}
	
	/*Handles a click event of a buttons*/
	
	private View.OnClickListener buttonsClickListener = new View.OnClickListener() {

		@Override
		public void onClick(View view) {
			
			if (view.getId() == R.id.button_search) {
				Intent intent = new Intent(Feed.this, SearchActivity.class);
				startActivity(intent);
			} else if (view.getId() == R.id.button_followers) {
				Intent intent = new Intent(Feed.this, FriendsActivity.class);
				intent.putExtra(FriendsActivity.FOLLOWERS_ONLY, true);
				startActivity(intent);
			} else if (view.getId() == R.id.button_browse) {
				Intent intent = new Intent(Feed.this, Browser.class);
				startActivityForResult(intent, Browser.ACTIVITY_RESULT);
			} else if (view.getId() == R.id.button_circle) {
				Intent intent = new Intent(Feed.this, CircleActivity.class);
				startActivity(intent);
			} else if (view.getId() == R.id.button_friends) {
				Intent intent = new Intent(Feed.this, FriendsActivity.class);
				startActivity(intent);
			} else if (view.getId() == R.id.button_logout) {
				
				APITalker.sharedTalker().logout(
						User.sharedUser(Feed.this).token(), null);
				User.sharedUser(Feed.this).logout();
				GcmUtils.removeRegistrationId(Feed.this);
				
				setResult(RESULT_OK);
				
				finish();
			} else if (view.getId() == R.id.button_friend_request) {
				Intent intent = new Intent(Feed.this, FriendRequestActivity.class);
				intent.setAction("FEED");
				
				intent.putExtra(Intent.EXTRA_TEXT, "FEED");
				startActivity(intent);
			} else if ( view.getId() == R.id.button_profile ) {
				
				Intent intent = new Intent(Feed.this, ProfileActivity.class);
				intent.putExtra("USER_ID", User.sharedUser(Feed.this).userId());
				startActivity(intent);
			}else if ( view.getId() == R.id.button_menu ) {
				if ( opened ){
					closeCommentBox();
				}
				
				RelativeLayout feedMainLayout = (RelativeLayout) findViewById(R.id.feed_main_layout);
				RelativeLayout underlyingRelativeLayout = (RelativeLayout) 
						findViewById(R.id.underlying_relative_layout);
				Button profileButton = (Button) findViewById(R.id.button_profile); 
				
				int moveWidth = feedMainLayout.getWidth()- view.getWidth();
				
				if(!movedRight){
					feedMainLayout.animate().x(moveWidth);
					underlyingRelativeLayout.animate().x(-view.getMeasuredWidth());
					movedRight = true;
					profileButton.setClickable(false);
					profileButton.setVisibility(View.INVISIBLE);
				}
				else{
					feedMainLayout.animate().x(0);
					underlyingRelativeLayout.animate().x(-underlyingRelativeLayout.getMeasuredWidth());
					movedRight = false;
					profileButton.setClickable(true);
					profileButton.setVisibility(View.VISIBLE);
				}
			}
		}	
	};
	
	/*
	 * Adapter
	 */
	
	
	
	public void openUrl( View view ){
		
		String commentUrl = getCommentViewUrl(view);
		String commentTags = getCommentViewUrl(view);
		ArrayList<Friend> commenterFriendsArray = returnedCheckedFriendsArray;
		
		Intent intent = new Intent(Feed.this, Browser.class);
		intent.setAction(ACTION_BROWSER);
		intent.putExtra(COMMENT_URL, commentUrl);
		intent.putExtra(COMMENT_TAGS, commentTags);
		intent.putParcelableArrayListExtra(COMMENTER_FRIENDS, commenterFriendsArray);
		startActivityForResult(intent, Browser.ACTIVITY_RESULT);
	}

	
	// GETS THE URL FROM INSIDE OF COMMENT VIEW , FOR THE VIEW WHICH IS IN THE COMMENT VIEW 
	public String getCommentViewUrl( View view ){
		Comment viewRealParent = getCommentObject(view);
		String url = viewRealParent.getUrl();
		return url;
	}
	
	// GETS THE TAGS FROM INSIDE OF COMMENT VIEW , FOR THE VIEW WHICH IS IN THE COMMENT VIEW 
		public String getCommentViewTags( View view ){
			Comment viewRealParent = getCommentObject(view);
			String tags = viewRealParent.getTags();
			return tags;
		}
		
		public String getCommentViewLikes( View view ){
			Comment viewRealParent = getCommentObject(view);
			String likesCount = Integer.toString( viewRealParent.getLikesCount() );
			return likesCount;
		}
	
	//GETS THE COMMENT OBJECT BY THE VIEW OF LIST ITEM , IN WHICH THIS COMMENT DISPLAYED
	public Comment getCommentObject( View view ){
		View parentView = (View)view.getParent();
		int position = feed.getPositionForView(parentView);
		Comment viewRealParent = adapter.getItem(position);
		return viewRealParent;			
	}
	
	public void onShowCommentBox(View view){
		opened  = true;
    	lastClickedCommentUrl =  getCommentViewUrl(view);
    	cancelButton.animate().alpha(1);
    	cancelButton.setVisibility(View.VISIBLE);
    	
    	commentBox.animate().x(0);
		commentBox.setVisibility(View.VISIBLE);
		
	}
	
	@Override
	public void feedCommentsSuccess( ArrayList<Comment> comments, ArrayList<String> profileImagesUrls ) {
		loadingDialog.dismiss();
		
		Log.d("COMMENTS SIZE", comments.size() +"");
		
		imageUrls.addAll(profileImagesUrls);
		
		
		Log.d("HERE I AM", comments.size() + "");
		
		if (comments.size() > 0) {
			adapter.addAllAtEnd(comments);
			
		}
		
		if (comments.size() < 25) {
			lockForever = true;
		}

		Log.d("NOW COMMETNS", adapter.getCount() + "");
		lockRequest = false;
	}
	
	@Override
	public void feedCommentsFailure(String error) {
		Helper.showToast(error, this, loadingDialog);
		lockRequest = false;
	}
	
	
	/*
	 * Broadcast Receiver
	 */
	
	private BroadcastReceiver gcmReciever = new BroadcastReceiver() {
		
		private final String USER = "user";
		private final String COMMENT_LIKE = "likes";
		
	    @Override
	    public void onReceive(Context context, Intent intent) {
	    	JSONObject user = null;
			try {
				user = new JSONObject( intent.getStringExtra( USER ) );
			} catch (JSONException e) {
				e.printStackTrace();
				return;
			}
			String commentLike = intent.getStringExtra( COMMENT_LIKE ); 
	    	int CommentLikeCount = Integer.parseInt( commentLike );
	    }
	};


	@Override

	public void commentSucceed(String comment, String id ) {
		loadingDialog.dismiss();
		
		User user = User.sharedUser(this);
		Friend friend = new Friend(
				user.username(),
				user.userId(),
				user.firstName(),
				user.lastName(),
				user.profileImageUri());
		Comment commentObject = new Comment(id, comment, lastClickedCommentUrl,
				friend, commentTagsString, 0, new Date());
		adapter.checkAndAdd( commentObject );
		comments.add( commentObject );
	}

	@Override
	public void commentFailure(String error) {
		Helper.showToast(error, this, loadingDialog);
	}
	
/*	@Override
	public void likeSucceed( int likesCount ) {
		likedComment.setLikesCount( likesCount );
		adapter.notifyDataSetChanged();
	}

	@Override
	public void likeFailed(String error) {
		Helper.showToast(error, this, loadingDialog);
	}*/
	
	/*
	 * LocationListener Methods
	 */

	@Override
	public void onLocationChanged(Location location) {
		Log.d("LOCATION", location.getLatitude() + " " + location.getLongitude());
		this.location = location;
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		
	}

	@Override
	public void onProviderEnabled(String provider) {
		Log.d("LOCATION", "ENABLED");
	}

	@Override
	public void onProviderDisabled(String provider) {
		Log.d("LOCATION", "DISABLED");
	}

	
	/*public void showToast(String text, Context context ) {
		int duration = Toast.LENGTH_SHORT;
		Toast toast = Toast.makeText(context, text, duration);
		toast.show();
		loadingDialog.dismiss();
	}*/
}