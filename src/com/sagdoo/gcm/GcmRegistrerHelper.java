package com.sagdoo.gcm;

import java.io.IOException;

import org.apache.http.Header;
import org.json.JSONArray;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.sagdoo.models.User;

public class GcmRegistrerHelper {
	private static final String GCM_PATH = "http://sagdooo.cypherincorporated.co.in/device";
	//private final static String GCM_PATH = "http://192.168.0.110/Dropbox/sagdoo/public";
	private static final String DEVICE_ID = "deviceId";
	
	private static final AsyncHttpClient http = new AsyncHttpClient();

	public static void registerForGcm(String token, final Activity activity) throws IOException {
		if (!GcmUtils.checkPlayServices(activity)) {
			return;
		}
		
		GcmUtils.gcm = GoogleCloudMessaging.getInstance(activity.getApplicationContext());
		
		String registrationId = GcmUtils.getRegistrationId(activity);
		
		if (registrationId.isEmpty()) {
			registerBackground(activity);
		}
	}
	
	private static void registerBackground(final Context context) {
		new AsyncTask<Void, String, String>() {
	        @Override
	        protected String doInBackground(Void... asyncParams) {
				String registrationId = "";
				try {
					GcmUtils.gcm.unregister();
					registrationId = GcmUtils.gcm.register(GcmUtils.SENDER_ID);
					Log.d("REGISTRATION ID", registrationId);
				} catch (IOException e) {
					e.printStackTrace();
					
					return "wtf";
				}
				
				return registrationId;
	        }
	        
	        @Override
	        protected void onPostExecute(final String result) {
	        	if (result.equals("wtf")) {
	        		return;
	        	}
	        	
				sendingToServer(result, context);
	        }
	    }.execute(null, null, null);
	}
	
	public static void sendingToServer(String result, final Context context) {
		RequestParams params = new RequestParams();
		params.put("id", User.sharedUser(context).userId());
		params.put(DEVICE_ID, result);
		
		Log.d("REGISTER GCM", "SENDING REGID");
		
		final String finalRegId = result;
		
		Log.d("REGISTER PARAMS", params.toString());
		
    	http.post(GCM_PATH, params, new JsonHttpResponseHandler() {
			@Override
			public void onSuccess(int statusCode, Header[] headers,
					JSONArray response) {
				Log.d("REGISTER GCM", "SUCCESS");
				Log.d("RESPONSE", response.toString());
				GcmUtils.storeRegistrationId(context, finalRegId);
			}

			@Override
			public void onFailure(int statusCode,
					org.apache.http.Header[] headers,
					java.lang.Throwable throwable,
					org.json.JSONArray errorResponse) {
				Log.d("REGISTER GCM", "SHIT HAPPENS " + statusCode);
			}
			
			@Override
			public void onFailure(int statusCode, org.apache.http.Header[] headers, byte[] responseBytes, java.lang.Throwable throwable) {
				Log.d("SUPER FAIL", "STATUS " + statusCode);
			}
		});
	}
}