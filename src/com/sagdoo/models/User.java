package com.sagdoo.models;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;

public class User {
	private final String USER_SHARED_FOLDER = "userSharedFolder";
	private final String USERNAME_KEY = "usernameKey";
	private final String PASSWORD_KEY = "passwordKey";
	private final String IS_LOGGED_IN_KEY = "isLoggedInKey";
	private final String TOKEN_KEY = "tokenKey";
	private final String USER_ID_KEY = "userIdKey";
	private final String LASTNAME_KEY = "lastNameKey";
	private final String FIRSTNAME_KEY = "firstNameKey";
	private final String PROFILE_IMAGE_URI_KEY = "profileImageKey";

	private static User user;

	private Context context;

	private String username;
	private String password;
	private String token;
	private String userId;
	private String firstName;
	private String lastName;
	private String profileImageUri;

	private boolean isLoggedIn;

	private User(Context context) {
		this.context = context;

		SharedPreferences preferences = context.getSharedPreferences(
				USER_SHARED_FOLDER, 0);
		username = preferences.getString(USERNAME_KEY, "");
		password = preferences.getString(PASSWORD_KEY, "");
		isLoggedIn = preferences.getBoolean(IS_LOGGED_IN_KEY, false);
		token = preferences.getString(TOKEN_KEY, "");
		firstName = preferences.getString(FIRSTNAME_KEY, "");
		lastName = preferences.getString(LASTNAME_KEY, "");
		profileImageUri = preferences.getString(PROFILE_IMAGE_URI_KEY, "");
		userId = preferences.getString(USER_ID_KEY, "");
	}

	public static User sharedUser(Context context) {
		if (user == null) {
			user = new User(context);
		}

		return user;
	}

	public void login(String username, String password, String userId,
			String token, String firstName, String lastName, String profileImageUri ) {
		this.username = username;
		this.password = password;
		this.isLoggedIn = true;
		this.token = token;
		this.firstName = firstName;
		this.lastName = lastName;
		this.userId = userId;
		this.profileImageUri = profileImageUri;

		SharedPreferences preferences = context.getSharedPreferences(
				USER_SHARED_FOLDER, 0);
		Editor editor = preferences.edit();
		editor.putString(USERNAME_KEY, username);
		editor.putString(PASSWORD_KEY, password);
		editor.putString(TOKEN_KEY, token);
		editor.putString(USER_ID_KEY, userId);
		editor.putString(LASTNAME_KEY, lastName);
		editor.putString(FIRSTNAME_KEY, firstName);
		editor.putString(PROFILE_IMAGE_URI_KEY, profileImageUri);
		editor.putBoolean(IS_LOGGED_IN_KEY, isLoggedIn);
		editor.commit();
	}

	public void logout() {
		this.isLoggedIn = false;

		SharedPreferences preferences = context.getSharedPreferences(
				USER_SHARED_FOLDER, 0);
		Editor editor = preferences.edit();
		editor.putBoolean(IS_LOGGED_IN_KEY, isLoggedIn);
		editor.commit();
	}

	public String userId() {
		return userId;
	}

	public String token() {
		return token;
	}

	public boolean isLoggedIn() {
		return isLoggedIn;
	}

	public String username() {
		return username;
	}

	public String password() {
		return password;
	}

	public String firstName() {
		return firstName;
	}

	public String lastName() {
		return lastName;
	}
	
	public String profileImageUri() {
		return profileImageUri;
	}
}