package com.sagdoo.models;

import android.os.Parcel;
import android.os.Parcelable;

public class Friend implements Parcelable {
	private String username;
	private String id;
	private String firstName;
	private String lastName;
	private String imageUrl;

	@Override
	public int describeContents() {
		return 0;
	}

	
    public void writeToParcel(Parcel out, int flags) {
        out.writeString(username);
        out.writeString(id);
        out.writeString(firstName);
        out.writeString(lastName);
        out.writeString(imageUrl);
    }

    public static final Parcelable.Creator<Friend> CREATOR
            = new Parcelable.Creator<Friend>() {
        public Friend createFromParcel(Parcel in) {
            return new Friend(in);
        }

        public Friend[] newArray(int size) {
            return new Friend[size];
        }
    };
    
    private Friend(Parcel in) {
        username = in.readString();
        id = in.readString();
        firstName = in.readString();
        lastName = in.readString();
        imageUrl = in.readString();
    }
	
	
	public Friend(String username, String id, String firstName, String lastName, String imageUrl) {
		this.username = username;
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.imageUrl = imageUrl;
	}
	
	public String imageUrl() {
		return imageUrl;
	}
	
	public String username() {
		return username;
	}
	
	public String id() {
		return id;
	}
	
	public String firstName() {
		return firstName;
	}
	
	public String lastName() {
		return lastName;
	}
}
	