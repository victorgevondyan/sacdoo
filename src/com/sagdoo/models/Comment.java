package com.sagdoo.models;

import java.util.Date;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

public class Comment implements Parcelable {
	String id;
	String description;
	String url;
	Friend friend;
	String commentTags;
	int likesCount;
	Date created;
	

	public Comment(String id, String description, String url, Friend friend,
			String commentCategory, int likesCount, Date created) {
		this.id = id;
		this.description = description;
		this.url = url;
		this.friend = friend;
		this.commentTags = commentCategory;
		this.created = created;
		this.likesCount = likesCount;
	}

	public String getId() {
		return id;
	}

	public String getDescription() {
		return description;
	}

	public String getUrl() {
		return url;
	}

	public Friend getFriend() {
		return friend;
	}
	
	public String getTags() {
		return commentTags;
	}

	public int describeContents() {
		return 0;
	}
	
	public int getLikesCount() {
		return likesCount;
	}
	
	public void setLikesCount( int likesCount) {
		 this.likesCount = likesCount;
	}
	
	public Date getDate() {
		return created;
	}
	

	public void writeToParcel(Parcel out, int flags) {
		out.writeString(id);
		out.writeString(description);
		out.writeString(url);
		out.writeParcelable(friend, 0);
		out.writeString(commentTags);
		out.writeSerializable(created);
		out.writeInt(likesCount);
	}

	public static final Parcelable.Creator<Comment> CREATOR = new Parcelable.Creator<Comment>() {
		public Comment createFromParcel(Parcel in) {
			return new Comment(in);
		}

		public Comment[] newArray(int size) {
			return new Comment[size];
		}
	};

	private Comment(Parcel in) {
		id = in.readString();
		description = in.readString();
		url = in.readString();
		friend = (Friend) in.readParcelable(Friend.class.getClassLoader());
		commentTags = in.readString();
		created = (Date) in.readSerializable();
		likesCount = in.readInt();
	}

}
