package com.sagdoo;

import java.util.ArrayList;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.sagdoo.helpers.FeedListAdapter;
import com.sagdoo.helpers.Helper;
import com.sagdoo.models.Comment;
import com.sagdoo.models.User;
import com.sagdoo.talkers.APITalker;
import com.sagdoo.talkers.FeedHandler;
import com.sagdoo.talkers.LikeHandler;

public class CircleActivity extends Activity implements LocationListener, FeedHandler, LikeHandler {
	private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10;
	private static final long MIN_TIME_BW_UPDATES = Long.MAX_VALUE;
	
	private LocationManager locationManager;
	private FeedListAdapter adapter;
	private ProgressDialog loadingDialog;
	private ArrayList<String> imageUrls = new ArrayList<String>();
	
	private Comment likedComment;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_circle);
		
		loadingDialog = new ProgressDialog(this);
		loadingDialog.setCancelable(false);
		loadingDialog.setCanceledOnTouchOutside(false);
		loadingDialog.show();
		
		try {
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            boolean isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            boolean isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

            if (isGPSEnabled == false && isNetworkEnabled == false) {
            	Log.d("NO PROVIDER", "PICHALKA");
            	loadingDialog.dismiss();
            	showNoLocationAccess();
            } else {
            	if (isGPSEnabled) {
                    locationManager.requestLocationUpdates(
                            LocationManager.GPS_PROVIDER,
                            MIN_TIME_BW_UPDATES,
                            MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                }
                if (isNetworkEnabled) {
                    locationManager.requestLocationUpdates(
                            LocationManager.NETWORK_PROVIDER,
                            MIN_TIME_BW_UPDATES,
                            MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                }
            }

        } catch (Exception e) {
        	Log.d("SHIT HAPPENS", "GEO");
        	showNoLocationAccess();
        	loadingDialog.dismiss();
            e.printStackTrace();
        }
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		return super.onOptionsItemSelected(item);
	}
	
	private void showNoLocationAccess() {
		hideNoComment();
		TextView noLocationAccess = (TextView) findViewById(R.id.no_location_access);
		
		if (adapter == null || adapter.getCount() == 0) {
			noLocationAccess.setVisibility(View.VISIBLE);
		}
	}
	
	private void hideNoLocationAccess() {
		TextView noLocationAccess = (TextView) findViewById(R.id.no_location_access);
		noLocationAccess.setVisibility(View.GONE);
	}
	
	private void showNoComment() {
		hideNoLocationAccess();
		TextView noComment = (TextView) findViewById(R.id.no_comments);
		noComment.setVisibility(View.VISIBLE);
	}
	
	private void hideNoComment() {
		TextView noComment = (TextView) findViewById(R.id.no_comments);
		noComment.setVisibility(View.GONE);
	}
	
	public void back(View view) {
		finish();
	}
	
	/*
	 * LocationListener Methods
	 */

	@Override
	public void onLocationChanged(Location location) {
		Log.d("LOCATION", location.getLatitude() + " " + location.getLongitude());

		if (location != null) {
			hideNoLocationAccess();
			APITalker.sharedTalker().getCommentsByGeo(
					User.sharedUser(this).token(),
					25,
					0,
					location.getLongitude(), 
					location.getLatitude(),
					this);
		}
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		
	}

	@Override
	public void onProviderEnabled(String provider) {
		Log.d("LOCATION", "ENABLED");
	}

	@Override
	public void onProviderDisabled(String provider) {
		Log.d("LOCATION", "DISABLED");
		if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
				&& !locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
			showNoLocationAccess();
		}
	}
	
	/*
	 * FeedHandler Methods
	 */

	@Override
	public void feedCommentsSuccess(ArrayList<Comment> comments, ArrayList<String> profileImagesUrls ) {
		loadingDialog.dismiss();
		
		
		
		if (comments.isEmpty()) {
			showNoComment();
		}
		
		Log.d("HERE IN CIRCLES", comments.toString());

		adapter = new FeedListAdapter(this, R.layout.feed_comment_item, comments, imageUrls );
		ListView list = (ListView) findViewById(R.id.feed);
		list.setAdapter(adapter);
		
		imageUrls.addAll( profileImagesUrls );
	}

	@Override
	public void feedCommentsFailure(String error) {
		loadingDialog.dismiss();
		Toast.makeText(this, error, Toast.LENGTH_LONG).show();
	}
	
	@Override
	public void likeSucceed( int likesCount ) {
		likedComment.setLikesCount( likesCount );
		adapter.notifyDataSetChanged();
	}

	@Override
	public void likeFailed(String error) {
		Helper.showToast(error, this, loadingDialog);
	}

}