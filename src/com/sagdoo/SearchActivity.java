package com.sagdoo;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.sagdoo.helpers.FeedListAdapter;
import com.sagdoo.helpers.Helper;
import com.sagdoo.models.Comment;
import com.sagdoo.models.User;
import com.sagdoo.talkers.APITalker;
import com.sagdoo.talkers.FeedSearchHandler;
import com.sagdoo.talkers.LikeHandler;

public class SearchActivity extends Activity implements FeedSearchHandler, TextWatcher, LikeHandler {
	private FeedListAdapter adapter;
	private ListView feed;
	EditText searchField;
	EditText tagField;
	
	String searchFieldText;
	String tagFieldText;
	
	private boolean lockRequest = false;
	private boolean lockForever = false;
	
	private ArrayList<Integer> requestIds = new ArrayList<Integer>();
	
	private String word = "";

	private ArrayList<String> imageUrls = new ArrayList<String>();
	
	private Comment likedComment;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_search);
		
		TextView searchIcon = (TextView) findViewById(R.id.search_icon);
		searchIcon.setTypeface(Typeface.createFromAsset(getAssets(), "fontawesome.ttf"));
		searchField = (EditText) findViewById(R.id.search_field);
		tagField = (EditText)findViewById(R.id.search_tag_field);
		
		searchField.addTextChangedListener(this);
		tagField.addTextChangedListener(this);
		
		adapter = new FeedListAdapter(this, R.layout.feed_comment_item, new ArrayList<Comment>(), imageUrls );
		feed = (ListView) findViewById(R.id.feed);
		feed.setAdapter(adapter);
		
		feed.setOnScrollListener(feedListScrollListener);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		return super.onOptionsItemSelected(item);
	}
	
	public OnScrollListener feedListScrollListener = new OnScrollListener() {

		@Override
		public void onScrollStateChanged(AbsListView view, int scrollState) {
			
		}

		@Override
		public void onScroll(AbsListView view, int firstVisibleItem,
				int visibleItemCount, int totalItemCount) {
			
			if ( firstVisibleItem >= totalItemCount - 10 && !lockRequest && !lockForever )
			{
				Log.d("ENTERING LISTS??", "YEH");
				APITalker talker = APITalker.sharedTalker();
				Context context = getApplicationContext();
				Log.d("SKIPING", adapter.getCount() + "");
				
				if (!word.isEmpty()) {
					requestIds.add(talker.getCommentsForSearch(User.sharedUser(context).token(), 25 , adapter.getCount() , searchFieldText, tagFieldText, SearchActivity.this));
				}
			}
		}
	};
	
	
	/*
	 * FeedHandler Methods
	 */

	@Override
	public void feedCommentsSuccess( ArrayList<Comment> comments, ArrayList<String> profileImagesUrls, int requestId) {
		if (!requestIds.contains(Integer.valueOf(requestId))) {
			return;
		}
		
		imageUrls.addAll( profileImagesUrls );
		
		Log.d("HERE I AM", comments.size() + "");
		
		if (comments.size() > 0) {
			adapter.addAllAtEnd(comments);
			
		}
		
		if (comments.size() < 25) {
			lockForever = true;
		}

		Log.d("NOW COMMETNS", adapter.getCount() + "");
		lockRequest = false;
	}

	@Override
	public void feedCommentsFailure(String error, int requestId) {
		if (!requestIds.contains(Integer.valueOf(requestId))) {
			return;
		}
		
		Context context = getApplicationContext();
		CharSequence text = error;
		int duration = Toast.LENGTH_SHORT;

		Toast toast = Toast.makeText(context, text, duration);
		toast.show();
		
		lockRequest = false;
	}
	
	@Override
	public void likeSucceed( int likesCount ) {
		likedComment.setLikesCount( likesCount );
		adapter.notifyDataSetChanged();
	}

	@Override
	public void likeFailed(String error) {
		Helper.showToast(error, this, null);
	}
	
	/*
	 * TextWatcher Methods
	 */

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count, int after) {
		
	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
		lockForever = false;
		lockRequest = true;
		
		searchFieldText = searchField.getText().toString();
		tagFieldText = tagField.getText().toString();
		
		word = s.toString();
		
		if (word.isEmpty()) {
			requestIds.clear();
			adapter.clear();
			return;
		}

		requestIds.clear();
		adapter.clear();
		requestIds.add(APITalker.sharedTalker().getCommentsForSearch(
				User.sharedUser(this).token(), 25, 0, searchFieldText,
				tagFieldText, this));
	}

	@Override
	public void afterTextChanged(Editable s) {
		
	}
	
	public void back(View view) {
		finish();
	}
}
