package com.sagdoo;

import java.util.LinkedList;
import java.util.Queue;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

class GesturedView extends RelativeLayout {
    Context context;
    GestureDetector gd;
    SwipeHandler handler;

    public GesturedView(Context context) {
        super(context);

        this.context = context;
        gd = new GestureDetector(context, sogl);
    }
    public GesturedView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        
        this.context = context;
        gd = new GestureDetector(context, sogl);
    }
    public GesturedView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        
        this.context = context;
        gd = new GestureDetector(context, sogl);
    }
    
    public void setSwipeHandler(SwipeHandler handler) {
    	this.handler = handler;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        Log.d("IS ACTION MOVE", event.getAction() == MotionEvent.ACTION_MOVE ? "YEH" : "NOP");
        return gd.onTouchEvent(event);
    }
    
    @Override
    public boolean dispatchTouchEvent(MotionEvent event)
    {
    	Log.d("NEEDS DISPATCH", "YES");

    	Queue<ViewGroup> children = new LinkedList<ViewGroup>();
    	children.add(this);
    	
    	while(!children.isEmpty()) {
    		ViewGroup view = children.poll();
    		
    		MotionEvent cp = MotionEvent.obtain(event);
//    		cp.offsetLocation(-view.getLeft(), -view.getTop());
    		view.onTouchEvent(cp);
    		cp.recycle();
//    		view.onTouchEvent(event);
    		
	    	for (int index = 0; index < view.getChildCount(); index++) {
	    		if (view.getChildAt(index) instanceof ViewGroup) {
		    		children.add((ViewGroup) view.getChildAt(index));
	    		} else {
	    			View child = view.getChildAt(index);
	    			MotionEvent cp2 = MotionEvent.obtain(event);
//	    			cp2.offsetLocation(-child.getLeft(), -child.getTop());
	    			child.onTouchEvent(cp2);
	    			cp2.recycle();
//	    			view.getChildAt(index).onTouchEvent(event);
	    		}
	    	}
    	}
    	
    	return true;
    }

    GestureDetector.SimpleOnGestureListener sogl = new GestureDetector.SimpleOnGestureListener() {
        public boolean onDown(MotionEvent event) {
        	Log.d("MOTION DOWN", "MOT DOWN");
            return true;
        }

        public boolean onFling(MotionEvent event1, MotionEvent event2, float velocityX, float velocityY) {
        	Log.d("HERE U AM", "YOH");
            if (event1.getRawX() > event2.getRawX()) {
                handler.swipeLeft();
                return true;
            }
            return false;
        }
    };

    void show_toast(final String text) {
        Toast t = Toast.makeText(context, text, Toast.LENGTH_SHORT);
        t.show();
    }
    
    /*
     * Handlers
     */
    
    public interface SwipeHandler {
    	public void swipeLeft();
    }
}