package com.sagdoo;

import java.util.ArrayList;
import java.util.Date;
import java.util.regex.Pattern;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.View.OnTouchListener;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.sagdoo.helpers.FeedListAdapter;
import com.sagdoo.helpers.Helper;
import com.sagdoo.models.Comment;
import com.sagdoo.models.Friend;
import com.sagdoo.models.User;
import com.sagdoo.talkers.APITalker;
import com.sagdoo.talkers.CommentHandler;
import com.sagdoo.talkers.FeedHandler;

public class Browser extends Activity implements OnKeyListener, CommentHandler,
		LocationListener, FeedHandler {
	public static int ACTIVITY_RESULT = 9900;
	public static final String COMMENTS_LIST = "commentsList";
	public static final String SAVED_URL = "savedUrl";
	public static final String ORIGINAL_SAVED_URL = "originalSavedUrl";
	public static final String COMMENT_BOX_OPENED = "commentBoxOpened";
	public static final String COMMENT_MESSAGE = "commentMessage";
	public static final String COMMENT_MESSAGE_SMALL = "commentMessageSmall";
	public static final String COMMENT_TAGS = "commentTags";
	public static final String COMMENT_URL = "commentUrl";
	public static final String COMMENT_LIKES = "commentLikes";
	public static final String COMMENTER_FRIENDS = "commenterFriends";

	public static final String ACTION_BROWSER = "actionBrowser";

	public static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 1;
	public static final long MIN_TIME_BW_UPDATES = 1;

	static final int ADD_FRIENDS_REQUEST_CODE = 1;

	private ProgressDialog loadingDialog;

	ArrayList<Friend> returnedCheckedFriendsArray = new ArrayList<Friend>();
	ArrayList<Friend> obtainedCheckedFriendsArray = new ArrayList<Friend>();

	ArrayList<Comment> comments = new ArrayList<Comment>();

	private RelativeLayout commentBox;
	private RelativeLayout draggable;
	private RelativeLayout fadeShadow;
	private LinearLayout commentContainer;

	private ListView feedList;

	private EditText edittext;
	private EditText commentMessageEditText;
	private EditText postMessageEditText;
	private EditText commentTagsEditText;
	private String commentTagsString;
	private WebView webView;
	private boolean opened = false;
	private boolean opening = false;

	private LocationManager locationManager;

	private Location location;

	private String obtainedClickedUrl = null;
	private String obtainedCommentTags = null;
	private String obtainedCommentLikes = null;

	private ArrayList<String> imageUrls = new ArrayList<String>();

	private Comment likedComment;

	Intent intent = new Intent();
	String intentAction;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_browser);

		commentBox = (RelativeLayout) findViewById(R.id.comment_box);
		draggable = (RelativeLayout) findViewById(R.id.draggable);
		commentContainer = (LinearLayout)findViewById(R.id.comment_container);
		fadeShadow = (RelativeLayout) findViewById(R.id.fade_shadow);
		draggable.setOnTouchListener(draggableOnTouchListener);
		fadeShadow.setAlpha(0);
		fadeShadow.setVisibility(View.VISIBLE);

		feedList = (ListView) findViewById(R.id.feed);
		webView = (WebView) findViewById(R.id.web_view);
		LinearLayout smallMessageLinear = (LinearLayout) findViewById(R.id.small_message_linear);

		// HANDLE AN INTENT RECEIVED FROM FEED ACTIVITY BY CLICKING ON COMMENT
		// URL
		intent = getIntent();
		intentAction = intent.getAction();
		obtainedClickedUrl = intent.getStringExtra(COMMENT_URL);
		obtainedCommentTags = intent.getStringExtra(COMMENT_TAGS);
		obtainedCheckedFriendsArray = intent
				.getParcelableArrayListExtra(COMMENTER_FRIENDS);

		if (obtainedClickedUrl == null) {
			LinearLayout.LayoutParams feedParams = (LinearLayout.LayoutParams) feedList
					.getLayoutParams();
			LinearLayout.LayoutParams webParams = (LinearLayout.LayoutParams) webView
					.getLayoutParams();
			feedParams.weight = 0;
			webParams.weight = 2;
			feedList.setLayoutParams(feedParams);
			webView.setLayoutParams(webParams);
			feedList.requestLayout();
			webView.requestLayout();

			findViewById(R.id.divider).setVisibility(View.GONE);
		} else {
			lockRequest = true;

			adapter = new FeedListAdapter(this, R.layout.feed_comment_item,
					new ArrayList<Comment>(), imageUrls);
			feedList.setAdapter(adapter);

			feedList.setOnScrollListener(feedListScrollListener);

			findViewById(R.id.toolbar).setVisibility(View.GONE);

			APITalker talker = APITalker.sharedTalker();
			talker.getComments(User.sharedUser(this).token(),
					obtainedClickedUrl, 25, 0, this);

			commentBox.setVisibility(View.GONE);
			smallMessageLinear.setVisibility(View.VISIBLE);

		}

		webView.getSettings().setJavaScriptEnabled(true);
		webView.setWebChromeClient(new WebChromeClient());
		webView.setWebViewClient(new WebViewClient());
		webView.loadUrl(obtainedClickedUrl == null
				|| obtainedClickedUrl.isEmpty() ? "http://www.google.com"
				: obtainedClickedUrl);

		commentMessageEditText = (EditText) findViewById(R.id.message);
		commentTagsEditText = (EditText) findViewById(R.id.comment_tag);
		postMessageEditText = (EditText) findViewById(R.id.small_message);
		edittext = (EditText) findViewById(R.id.browse_link);
		edittext.setOnKeyListener(this);
		edittext.setText(obtainedClickedUrl);

		Typeface fontawesome = Typeface.createFromAsset(getAssets(),
				"fontawesome.ttf");

		TextView backwardTextView = (TextView) findViewById(R.id.backward);
		TextView forwardTextView = (TextView) findViewById(R.id.forward);
		TextView commentTextView = (TextView) findViewById(R.id.comment);
		TextView homeTextView = (TextView) findViewById(R.id.home);
		TextView closeTextView = (TextView) findViewById(R.id.close);

		backwardTextView.setTypeface(fontawesome);
		commentTextView.setTypeface(fontawesome);
		forwardTextView.setTypeface(fontawesome);
		homeTextView.setTypeface(fontawesome);
		closeTextView.setTypeface(fontawesome);

		TextView addFriendsButton = (TextView) findViewById(R.id.add_friends);
		TextView sendMessageButton = (TextView) findViewById(R.id.send_message);
		TextView cancelButton = (TextView) findViewById(R.id.cancel);
		addFriendsButton.setTypeface(fontawesome);
		sendMessageButton.setTypeface(fontawesome);
		cancelButton.setTypeface(fontawesome);

		TextView friendsTagged = (TextView) findViewById(R.id.tagged_friends);
		friendsTagged.setText("Tagged friends: none");

		loadingDialog = new ProgressDialog(this);

		closeTextView.setOnClickListener(closeOnClickListener);

		if (savedInstanceState != null) {

			String obtainedUrlString = savedInstanceState.getString(SAVED_URL);
			webView.loadUrl(obtainedUrlString);

			String obtainedOriginalUrlString = savedInstanceState
					.getString(ORIGINAL_SAVED_URL);
			edittext.setText(obtainedOriginalUrlString);

			opened = savedInstanceState.getBoolean(COMMENT_BOX_OPENED);

			String obtainedCommentMessage = savedInstanceState
					.getString(COMMENT_MESSAGE);
			commentMessageEditText.setText(obtainedCommentMessage);

			String obtainedCommentTags = savedInstanceState
					.getString(COMMENT_TAGS);
			commentTagsEditText.setText(obtainedCommentTags);

			if ( intentAction == ACTION_BROWSER ) {
				String obtainedCommentMessageSmall = savedInstanceState
						.getString(COMMENT_MESSAGE_SMALL);
				postMessageEditText.setText(obtainedCommentMessageSmall);
			}
		}

		try {
			locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
			boolean isGPSEnabled = locationManager
					.isProviderEnabled(LocationManager.GPS_PROVIDER);
			boolean isNetworkEnabled = locationManager
					.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

			if (isGPSEnabled == false && isNetworkEnabled == false) {
				Log.d("NO PROVIDER", "PICHALKA");
			} else {
				if (isGPSEnabled) {
					locationManager.requestLocationUpdates(
							LocationManager.GPS_PROVIDER, MIN_TIME_BW_UPDATES,
							MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
				}
				if (isNetworkEnabled) {
					locationManager.requestLocationUpdates(
							LocationManager.NETWORK_PROVIDER,
							MIN_TIME_BW_UPDATES,
							MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
				}
			}

		} catch (Exception e) {
			Log.d("SHIT HAPPENS", "GEO");
			e.printStackTrace();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onSaveInstanceState(android.os.Bundle)
	 */
	@Override
	protected void onSaveInstanceState(Bundle savedInstanceState) {
		String savedUrlString = webView.getUrl();
		savedInstanceState.putString(SAVED_URL, savedUrlString);

		String savedOriginalUrlString = edittext.getText().toString();
		savedInstanceState.putString(ORIGINAL_SAVED_URL, savedUrlString);

		savedInstanceState.putBoolean(COMMENT_BOX_OPENED, opened);

		String commentMesage = commentMessageEditText.getText().toString();
		savedInstanceState.putString(COMMENT_MESSAGE, commentMesage);

		savedInstanceState.putString(COMMENT_TAGS, commentTagsString);

		if ( intentAction == ACTION_BROWSER ) {
			String commentMessageSmall = postMessageEditText.getText()
					.toString();
			savedInstanceState
					.putString(COMMENTER_FRIENDS, commentMessageSmall);
		}

		super.onSaveInstanceState(savedInstanceState);
	}

	@Override
	public void onBackPressed() {
		if (opened) {
			opened = false;

			TextView commentText = (TextView) findViewById(R.id.comment);
			TextView cancelText = (TextView) findViewById(R.id.cancel);
			commentText.animate().alpha(1);
			cancelText.animate().alpha(0);
			commentBox.animate()
					.x(commentBox.getWidth() - draggable.getWidth());
		} else {
			super.onBackPressed();
		}
	}

	public OnClickListener closeOnClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {

			EditText edittext = (EditText) findViewById(R.id.browse_link);
			edittext.setText("");

		}
	};

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		if (hasFocus && commentBox.getVisibility() == View.INVISIBLE && !opened) {
			commentBox.setX(commentBox.getWidth() - draggable.getWidth());
		}
		if ( intentAction != ACTION_BROWSER) {
			commentBox.setVisibility(View.VISIBLE);
		}
	}

	@Override
	public void finish() {
		Log.d("COMMENTS LSIST", comments.toString());
		Intent result = new Intent();
		result.putParcelableArrayListExtra(COMMENTS_LIST, comments);
		setResult(Activity.RESULT_OK, result);

		super.finish();
	}

	/*
	 * Navigation Handlers
	 */

	public void goHome(View view) {
		finish();
	}

	/*
	 * Browser Navigation Handlers
	 */

	public void browserBackward(View view) {
		WebView webView = (WebView) findViewById(R.id.web_view);

		if (webView.copyBackForwardList().getCurrentIndex() > 0) {
			webView.goBack();
		}
	}

	public void browserForward(View view) {
		WebView webView = (WebView) findViewById(R.id.web_view);

		if (webView.copyBackForwardList().getCurrentIndex() < webView
				.copyBackForwardList().getSize() - 1) {
			webView.goForward();
		}
	}

	/*
	 * Commenting Methods
	 */

	public void sendMessage(View view) {
		EditText messageText = (EditText) findViewById(R.id.message);
		WebView webView = (WebView) findViewById(R.id.web_view);

		commentTagsString = commentTagsEditText.getText().toString();
		String message = messageText.getText().toString();
		String url = webView.getUrl();

		if (message.length() < 1 || url == null || url.length() == 0) {
			Toast.makeText(this, "No comment or no page", Toast.LENGTH_SHORT)
					.show();
			loadingDialog.dismiss();
			return;
		}

		messageText.setText("");
		loadingDialog.setCancelable(false);
		loadingDialog.setCanceledOnTouchOutside(false);
		loadingDialog.show();

		int length = returnedCheckedFriendsArray.size();

		String[] returnedFriendsIndexesArray = new String[length];
		int i;
		for (i = 0; i < length; i++) {
			returnedFriendsIndexesArray[i] = returnedCheckedFriendsArray.get(i)
					.id();
		}

		// new AsyncTask<Void, Void, Void>() {
		//
		// @Override
		// protected Void doInBackground(Void... params) {
		// GcmUtils.sendMessage("URL_MY", "MESSAGE_MY");
		// return null;
		// }
		//
		// }.execute(null, null, null);
		if (!commentTagsString.matches("([a-zA-Z]*)([,][a-zA-Z]*)*")
				&& (intentAction != ACTION_BROWSER)) {
			Toast.makeText(this, "ILLEGAL TAG(S) DESCRIPTION",
					Toast.LENGTH_SHORT).show();
			loadingDialog.dismiss();
			return;
		}
		APITalker.sharedTalker().addComment(User.sharedUser(this).token(), url,
				location == null ? -1 : location.getLongitude(),
				location == null ? -1 : location.getLatitude(), message,
				returnedFriendsIndexesArray, commentTagsString, this);
	}

	public void onPost(View view) {

		EditText messageText = (EditText) findViewById(R.id.small_message);
		WebView webView = (WebView) findViewById(R.id.web_view);

		commentTagsString = obtainedCommentTags;
		String message = messageText.getText().toString();
		String url = webView.getUrl();

		if (message.length() < 1 || url == null || url.length() == 0) {
			Toast.makeText(this, "No comment or no page", Toast.LENGTH_SHORT)
					.show();
			loadingDialog.dismiss();
			return;
		}

		messageText.setText("");
		loadingDialog.setCancelable(false);
		loadingDialog.setCanceledOnTouchOutside(false);
		loadingDialog.show();

		int length = obtainedCheckedFriendsArray.size();

		String[] obtainedFriendsIndexesArray = new String[length];
		int i;
		for (i = 0; i < length; i++) {
			obtainedFriendsIndexesArray[i] = obtainedCheckedFriendsArray.get(i)
					.id();
		}

		// new AsyncTask<Void, Void, Void>() {
		//
		// @Override
		// protected Void doInBackground(Void... params) {
		// GcmUtils.sendMessage("URL_MY", "MESSAGE_MY");
		// return null;
		// }
		//
		// }.execute(null, null, null);
		if (!commentTagsString.matches("([a-zA-Z]*)([,][a-zA-Z]*)*")
				&& (intentAction != ACTION_BROWSER)) {
			Toast.makeText(this, "ILLEGAL TAG(S) DESCRIPTION",
					Toast.LENGTH_SHORT).show();
			loadingDialog.dismiss();
			return;
		}
		APITalker.sharedTalker().addComment(User.sharedUser(this).token(), url,
				location == null ? -1 : location.getLongitude(),
				location == null ? -1 : location.getLatitude(), message,
				obtainedFriendsIndexesArray, commentTagsString, this);

	}

	/*
	 * Friends Methods
	 */

	public void addFriends(View view) {
		Intent friendsIntent = new Intent(this, AddFriendsActivity.class);
		startActivityForResult(friendsIntent, ADD_FRIENDS_REQUEST_CODE);

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == ADD_FRIENDS_REQUEST_CODE
				&& resultCode == Activity.RESULT_OK) {
			returnedCheckedFriendsArray = data
					.getParcelableArrayListExtra(AddFriendsActivity.CHECKED_FRIENDS_LIST);

			TextView friendsTagged = (TextView) findViewById(R.id.tagged_friends);
			String tagged = "Tagged friends: ";

			for (int index = 0; index < returnedCheckedFriendsArray.size(); index++) {
				tagged = tagged
						+ returnedCheckedFriendsArray.get(index).firstName()
						+ " "
						+ returnedCheckedFriendsArray.get(index).lastName();

				if (index != returnedCheckedFriendsArray.size() - 1) {
					tagged = ", ";
				}
			}

			if (returnedCheckedFriendsArray.size() == 0) {
				tagged = tagged + "none";
			}

			friendsTagged.setText(tagged);
		}
	}

	/*
	 * OnKeyListener Methods
	 */

	@Override
	public boolean onKey(View v, int keyCode, KeyEvent event) {
		if ((event.getAction() == KeyEvent.ACTION_DOWN)
				&& (keyCode == KeyEvent.KEYCODE_ENTER)) {
			EditText edittext = (EditText) findViewById(R.id.browse_link);
			String urlLink = edittext.getText().toString();
			String finalUrlLink = urlLink;
			Pattern pattern = Pattern
					.compile("^(http://|https://)?(www.)?([a-zA-Z0-9]+).[a-zA-Z0-9]*.[a-z]{3}.?([a-z]+)?$");

			if (!urlLink.contains("http://") && !urlLink.contains("https://")) {
				finalUrlLink = "http://" + urlLink;
			}

			if (!pattern.matcher(finalUrlLink).matches()) {
				finalUrlLink = "http://google.com/search?q=" + urlLink;
			}

			WebView webView = (WebView) findViewById(R.id.web_view);
			webView.loadUrl(finalUrlLink);

			return true;
		}

		return false;
	}

	/*
	 * CommentHandler Methods
	 */

	@Override
	public void commentSucceed(String comment, String id) {
		Helper.showToast("ADD COMMENT SUCCEED", this, loadingDialog);
		
		loadingDialog.dismiss();

		WebView webView = (WebView) findViewById(R.id.web_view);

		String url = webView.getUrl();
		User user = User.sharedUser(this);
		Friend friend = new Friend(user.username(), user.userId(),
				user.firstName(), user.lastName(), User.sharedUser(this)
						.profileImageUri());
		Comment commentObject = new Comment(id, comment, url, friend,
				commentTagsString, 0, new Date());
		if (adapter != null) {
			adapter.checkAndAdd(commentObject);
		}

		comments.add(commentObject);
	}

	@Override
	public void commentFailure(String error) {
		loadingDialog.dismiss();

		Context context = getApplicationContext();
		CharSequence text = error;
		int duration = Toast.LENGTH_SHORT;

		Toast toast = Toast.makeText(context, text, duration);
		toast.show();
	}

	public void showSoftKeyboard(View view) {
		if (view.requestFocus()) {
			InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
		}
	}

	/*
	 * Draggable OnTouch Listener
	 */

	private OnTouchListener draggableOnTouchListener = new OnTouchListener() {
		private boolean isDragging = false;
		private int prevX;

		@Override
		public boolean onTouch(View view, MotionEvent event) {
			int curX = (int) event.getRawX();
			int diffX = 0;

			if (event.getAction() == MotionEvent.ACTION_DOWN) {
				if (isDragging) {
					return false;
				}

				prevX = curX;

				if (opened) {
					isDragging = true;
					opened = false;

					TextView commentText = (TextView) findViewById(R.id.comment);
					TextView cancelText = (TextView) findViewById(R.id.cancel);
					commentText.setVisibility(View.VISIBLE);
					commentText.animate().alpha(1);
					cancelText.animate().alpha(0);

					commentBox.animate().setListener(dragAnimationListener);
					commentBox.animate().x(
							commentBox.getWidth() - draggable.getWidth());
				}

				return true;
			} else if (event.getAction() == MotionEvent.ACTION_MOVE) {
				if (!isDragging && !opened) {
					diffX = curX - prevX;
					prevX = curX;

					int dest = (int) commentBox.getX() + diffX;

					if (dest < 0
							|| dest > commentBox.getWidth()
									- draggable.getWidth()) {
						return true;
					}

					if (dest < 3 * (commentBox.getWidth() - draggable
							.getWidth()) / 4) {
						isDragging = true;
						commentBox.animate().setListener(dragAnimationListener);
						commentBox.animate().x(0);

						TextView commentText = (TextView) findViewById(R.id.comment);
						TextView cancelText = (TextView) findViewById(R.id.cancel);
						cancelText.setAlpha(0);
						cancelText.setVisibility(View.VISIBLE);
						commentText.animate().alpha(0);
						cancelText.animate().alpha(1);

						// opened = true;
						opening = true;

						return true;
					}

					commentBox.setX(dest);
					commentBox.invalidate();

					return true;
				}
			} else if (event.getAction() == MotionEvent.ACTION_UP) {
				if (!isDragging && !opened) {
					isDragging = false;
					commentBox.animate().setListener(dragAnimationListener);
					commentBox.animate().x(
							commentBox.getWidth() - draggable.getWidth());
				}

				return false;
			}

			return false;
		}

		private AnimatorListener dragAnimationListener = new AnimatorListener() {

			@Override
			public void onAnimationStart(Animator animation) {

			}

			@Override
			public void onAnimationEnd(Animator animation) {
				isDragging = false;

				if (opening) {
					fadeShadow.setAlpha(0.4f);
					opening = false;
					opened = true;
					TextView commentText = (TextView) findViewById(R.id.comment);
					commentText.setVisibility(View.INVISIBLE);
				} else {
					fadeShadow.setAlpha(0f);
				}
			}

			@Override
			public void onAnimationCancel(Animator animation) {

			}

			@Override
			public void onAnimationRepeat(Animator animation) {

			}
		};
	};

	/*
	 * LocationListener Methods
	 */

	@Override
	public void onLocationChanged(Location location) {
		Log.d("LOCATION",
				location.getLatitude() + " " + location.getLongitude());
		this.location = location;
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {

	}

	@Override
	public void onProviderEnabled(String provider) {
		Log.d("LOCATION", "ENABLED");
	}

	@Override
	public void onProviderDisabled(String provider) {
		Log.d("LOCATION", "DISABLED");
	}

	/*
	 * Same link comments list code View here only after a cup of bear :) :'(
	 */

	private boolean lockRequest = false;
	private boolean lockForever = false;
	private FeedListAdapter adapter;

	public OnScrollListener feedListScrollListener = new OnScrollListener() {

		@Override
		public void onScrollStateChanged(AbsListView view, int scrollState) {

		}

		@Override
		public void onScroll(AbsListView view, int firstVisibleItem,
				int visibleItemCount, int totalItemCount) {

			if (firstVisibleItem >= totalItemCount - 10 && !lockRequest
					&& !lockForever) {
				Log.d("ENTERING LISTS??", "YEH");
				APITalker talker = APITalker.sharedTalker();
				Context context = getApplicationContext();
				Log.d("SKIPING", adapter.getCount() + "");
				talker.getComments(User.sharedUser(context).token(),
						obtainedClickedUrl, 25, adapter.getCount(),
						Browser.this);
			}
		}
	};

	/*
	 * Adapter
	 */

	public void feedCommentsSuccess(ArrayList<Comment> comments,
			ArrayList<String> profileImagesUrls) {
		loadingDialog.dismiss();

		imageUrls.addAll(profileImagesUrls);

		Log.d("HERE I AM", comments.size() + "");

		if (comments.size() > 0) {
			adapter.addAllAtEnd(comments);

		}

		if (comments.size() < 25) {
			lockForever = true;
		}

		Log.d("NOW COMMETNS", adapter.getCount() + "");
		lockRequest = false;
	}

	@Override
	public void feedCommentsFailure(String error) {
		loadingDialog.dismiss();

		Context context = getApplicationContext();
		CharSequence text = error;
		int duration = Toast.LENGTH_SHORT;

		Toast toast = Toast.makeText(context, text, duration);
		toast.show();

		lockRequest = false;
	}

	/*
	 * @Override public void likeSucceed( int likesCount ) {
	 * likedComment.setLikesCount( likesCount ); adapter.notifyDataSetChanged();
	 * }
	 * 
	 * @Override public void likeFailed(String error) { Helper.showToast(error,
	 * this, loadingDialog); }
	 */
}