package com.sagdoo;

import java.util.ArrayList;

import android.app.Activity;
import android.app.ProgressDialog;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.sagdoo.models.Friend;
import com.sagdoo.models.User;
import com.sagdoo.talkers.APITalker;
import com.sagdoo.talkers.FriendsHandler;

public class FriendsActivity extends Activity implements FriendsHandler {
	public static final String FOLLOWERS_ONLY = "followersOnly";
	
	ProgressDialog loadingDialog;
	
	public AddFriendsAdapter adapter;
	
	private boolean lockRequests = false;
	private boolean lockForever = false;
	
	private boolean followersOnly = false;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_friends);
		
		loadingDialog = new ProgressDialog(this);
		loadingDialog.setCancelable(false);
		loadingDialog.setCanceledOnTouchOutside(false);
		loadingDialog.show();
		
		followersOnly = getIntent().getBooleanExtra(FOLLOWERS_ONLY, false);
		
		if (followersOnly) {
			TextView noFriendsTextView = (TextView) findViewById(R.id.no_friends);
			noFriendsTextView.setText(R.string.no_followers);
		}
		
		lockRequests = true;
		APITalker.sharedTalker().getFriends(
				User.sharedUser(this).userId(), followersOnly, 25, 0, 1,
				this);
		
		adapter = new AddFriendsAdapter(this, R.layout.add_friend_item, new ArrayList<Friend>());
		ListView listView = (ListView) findViewById(R.id.friends);
		listView.setAdapter(adapter);
		
		listView.setOnScrollListener(friendsListScrollListener);
	}
	
	public OnScrollListener friendsListScrollListener = new OnScrollListener() {

		@Override
		public void onScrollStateChanged(AbsListView view, int scrollState) {
			
		}

		@Override
		public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
			if ( firstVisibleItem >= totalItemCount - 10 && !lockRequests && !lockForever ) {
				APITalker talker = APITalker.sharedTalker();
				Context context = getApplicationContext();
				talker.getFriends(
						User.sharedUser(context).userId(), followersOnly, 25, adapter.getCount(), 1,
						FriendsActivity.this);
			}
		}
	};

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		return super.onOptionsItemSelected(item);
	}
	
	public void addNewFriends(View view) {
		Intent intent = new Intent(this, NewFriendsActivity.class);
		intent.setAction("NEW_FRIENDS");
		intent.putExtra(Intent.EXTRA_TEXT, "NEW_FRIENDS");
		startActivity(intent);
	}
	
	/*
	 * ListView Adapter
	 */
	
	private class AddFriendsAdapter extends ArrayAdapter<Friend> {
		private Context context;
		private LayoutInflater inflater;
		private ArrayList<Friend> friends;
		private int resource;
		
		public AddFriendsAdapter(Context context, int resource, ArrayList<Friend> friends) {
			super(context, resource, friends);
			
			this.context = context;
			this.resource = resource;
			this.friends = friends;
			this.inflater = (LayoutInflater) context.getSystemService(Service.LAYOUT_INFLATER_SERVICE);
		}
		
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			if (convertView == null) {
				convertView = inflater.inflate(resource, parent, false);
				
				TextView addIcon = (TextView) convertView.findViewById(R.id.add_icon);
				//addIcon.setTypeface(Typeface.createFromAsset(context.getAssets(), "fontawesome.ttf"));
			}
			
			Friend friend = getItem(position);
			TextView name = (TextView) convertView.findViewById(R.id.name);
			name.setText(friend.firstName() + " " + friend.lastName());
			
			return convertView;
		}
		
		@Override
		public Friend getItem(int position) {
			return friends.get(position);
		}
		
		@Override
		public int getCount() {
			return friends.size();
		}
		
		public void addAtEnd(ArrayList<Friend> newFriends) {
			friends.addAll(newFriends);
			notifyDataSetChanged();
		}
	}
	
	/*
	 * FriendsHandler Methods
	 */

	@Override
	public void getFriendsSuccess(ArrayList<Friend> friends) {
		TextView noFriendsTextView = (TextView) findViewById(R.id.no_friends);
		if( friends.isEmpty() )
		noFriendsTextView.setVisibility(View.VISIBLE);
		
		loadingDialog.dismiss();
		
		if (friends.size() > 0) {
			adapter.addAtEnd(friends);
		}
		
		if (friends.size() < 25) {
			lockForever = true;
		}
		
		lockRequests = false;
	}

	@Override
	public void getFriendsFailure(String error) {
		loadingDialog.dismiss();
		
		Context context = getApplicationContext();
		CharSequence text = error;
		int duration = Toast.LENGTH_SHORT;

		Toast toast = Toast.makeText(context, text, duration);
		toast.show();
		
		lockRequests = false;
	}
	
	public void onBack(View view){
		finish();
	}
}