package com.sagdoo.talkers;

public interface LoginHandler {
	public void loginSucceed(String username, String password, String userId, String token, String firstName, String lastName, String profileImageUri );
	public void loginFailed(String error);
}
