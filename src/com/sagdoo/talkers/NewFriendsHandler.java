package com.sagdoo.talkers;


public interface NewFriendsHandler {
	
	public void addNewFriendsSucceed();
	public void addNewFriendsFailed(String error);

}
