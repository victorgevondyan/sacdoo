package com.sagdoo.talkers;

public interface UploadImageHandler {
	public void uploadImageSucceed(String profileImageUri);

	public void uploadImageFailed(String error);
}
