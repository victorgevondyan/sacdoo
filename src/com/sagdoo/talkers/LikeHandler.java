package com.sagdoo.talkers;

public interface LikeHandler {
	public void likeSucceed( int likeCount );
	public void likeFailed( String error );
}
