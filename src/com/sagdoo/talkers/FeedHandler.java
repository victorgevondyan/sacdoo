package com.sagdoo.talkers;

import java.util.ArrayList;

import com.sagdoo.models.Comment;

public interface FeedHandler {
	public void feedCommentsSuccess( ArrayList<Comment> comments, ArrayList<String> profileImagesUris );
	public void feedCommentsFailure(String error);
}
