package com.sagdoo.talkers;

import java.util.ArrayList;

import com.sagdoo.models.Friend;

public interface UsersHandler {
	public void getUsersSucceed(ArrayList<Friend> friends, int requestId);
	public void getUsersFailed(String error, int requestId);
}