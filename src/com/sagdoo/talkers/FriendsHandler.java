package com.sagdoo.talkers;

import java.util.ArrayList;

import com.sagdoo.models.Friend;

public interface FriendsHandler {
	public void getFriendsSuccess(ArrayList<Friend> friends);
	public void getFriendsFailure(String error);
}
