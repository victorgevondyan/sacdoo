package com.sagdoo.talkers;

public interface LogoutHandler {
	public void logoutSucceed(String token);
	public void logoutFailure(String error);
}
