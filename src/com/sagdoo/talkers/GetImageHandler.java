package com.sagdoo.talkers;

public interface GetImageHandler {
	public void getImageSucceed( String profileImageUri );

	public void getImageFailed( String error );

}
