package com.sagdoo.talkers;

import java.util.ArrayList;

import com.sagdoo.models.Comment;

public interface FeedSearchHandler {
	public void feedCommentsSuccess( ArrayList<Comment> comments, ArrayList<String> profileImagesUris, int requestId );
	public void feedCommentsFailure(String error, int requestId);
}
