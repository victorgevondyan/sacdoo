package com.sagdoo.talkers;

public interface RegisterHandler {
	public void registerSucceed(String username, String password, String userId, String token, String firstName, String lastName, String profileImageUri );
	public void registerFailed(String error);
}
