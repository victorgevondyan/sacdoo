package com.sagdoo.talkers;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.FileAsyncHttpResponseHandler;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.sagdoo.ProfileActivity;
import com.sagdoo.models.Comment;
import com.sagdoo.models.Friend;
import com.sagdoo.models.User;

public class APITalker {
	private final static String BASE_URL = "http://sagdooo.cypherincorporated.co.in";
	//private final static String BASE_URL = "http://192.168.0.110/Dropbox/sagdoo/public";
	private final String LOGIN_URL = BASE_URL + "/login";
	private final String LOGOUT_URL = BASE_URL + "/logout";
	private final String USER_URL = BASE_URL + "/user";
	private final String FOLLOWRS_URL = BASE_URL + "/follow";
	private final String COMMENT_URL = BASE_URL + "/comment";
	private final String COMMENT_BY_GEO_URL = BASE_URL + "/commentByGeolocation";
	private final String COMMENT_SEARCH_URL = BASE_URL + "/commentSearch";
	private final String COMMENT_LIKE_URL = BASE_URL + "/like";
	private final String FRIEND_URL = BASE_URL + "/friend";
	private final String UPLOAD_IMAGE_URL = BASE_URL + "/image";

	private final String USERNAME = "username";
	private final String PASSWORD = "password";
	private final String FIRST_NAME = "firstName";
	private final String LAST_NAME = "lastName";
	private final String EMAIL = "email";
	private final String MOBILE = "mobile";
	private final String RECEIVED_PROFILE_IMAGE_URI = "image";
	private final String PROFILE_IMAGE = "profileImage";
	private final String COMMENT_LIKES = "likes";
	private final String COMMENT_ID = "commentId";
	private final String LIMIT = "limit";
	private final String SKIP = "skip";
	private final String LONG = "lng";
	private final String LAT = "lat";
	private final String WORD = "word";

	private final String TOKEN = "token";
	private final String ID = "id";
	private final String FRIEND_ID = "friendId";
	private final String USER = "user";
	private final String FRIENDS_IDS = "friendIds";
	private final String STATUS = "approvedStatus";

	private final String URL = "url";
	private final String DESCRIPTION = "description";
	private final String TAGS = "tags";
	private final String TAG = "tag";

	private final int LIMIT_DEFAULT = 25;

	private static APITalker talker;

	private AsyncHttpClient http;

	private APITalker() {
		http = new AsyncHttpClient();
		http.setTimeout(5000);
		http.setMaxRetriesAndTimeout(5, 500);
	}

	public static APITalker sharedTalker() {
		if (talker == null) {
			talker = new APITalker();
		}

		return talker;
	}

	public void login(final String username, final String password,
			final LoginHandler handler) {
		RequestParams params = new RequestParams();
		params.put(USERNAME, username);
		params.put(PASSWORD, password);

		params.setHttpEntityIsRepeatable(true);
		// HttpUtils.getAsyncHttpClient().allowRetryExceptionClass(IOException.class);
		// HttpUtils.getAsyncHttpClient().allowRetryExceptionClass(IllegalArgumentException.class);
		// HttpUtils.getAsyncHttpClient().allowRetryExceptionClass(ConnectTimeoutException.class);
		// HttpUtils.getAsyncHttpClient().blockRetryExceptionClass(UnknownHostException.class);
		// HttpUtils.getAsyncHttpClient().blockRetryExceptionClass(ConnectionPoolTimeoutException.class);
		// HttpUtils.getAsyncHttpClient().setMaxRetriesAndTimeout(5,1000);

		http.post(LOGIN_URL, params, new JsonHttpResponseHandler() {
			@Override

			public void onSuccess(int statusCode, Header[] headers,
					JSONArray response) {
				Log.d("LOGIN RESPONSE", response.toString());
				if (handler != null) {

					String token = response.optJSONObject(1).optString(TOKEN);
					String id = response.optJSONObject(1).optString(ID);
					String firstName = response.optJSONObject(1).optString(
							FIRST_NAME);
					String lastName = response.optJSONObject(1).optString(
							LAST_NAME);
					String profileImageUri = getCleanedUri(response);
					Log.d("IMAGE URL ON LOGIN", profileImageUri);
					handler.loginSucceed(username, password, id, token,
							firstName, lastName, profileImageUri);
				}
			}

			@Override
			public void onFailure(int statusCode,
					org.apache.http.Header[] headers,
					java.lang.Throwable throwable,
					org.json.JSONArray errorResponse) {
				if (handler != null) {
					String error = errorResponse.optString(1);
					handler.loginFailed(error.isEmpty() ? "Unable to login. Please, try later."
							: error);
				}
			}

			@Override
			public void onFailure(int statusCode,
					org.apache.http.Header[] headers, byte[] responseBytes,
					java.lang.Throwable throwable) {
				Log.d("SUPER FAIL", "STATUS " + statusCode);
				if (handler != null) {
					handler.loginFailed("Unable to login. Please, try later.");
				}
			}
		});
	}

	public void logout(final String token, final LogoutHandler handler) {
		RequestParams params = new RequestParams();
		params.put(TOKEN, token);

		http.post(LOGOUT_URL, params, new JsonHttpResponseHandler() {

			@Override
			public void onSuccess(int statusCode, Header[] headers,
					JSONArray response) {
				Log.d("LOGIN RESPONSE", response.toString());
				if (handler != null) {

					String token = response.optJSONObject(1).optString(TOKEN);
					handler.logoutSucceed(token);
				}
			}

			@Override
			public void onFailure(int statusCode,
					org.apache.http.Header[] headers,
					java.lang.Throwable throwable,
					org.json.JSONArray errorResponse) {
				if (handler != null) {
					String error = errorResponse.optString(1);
					handler.logoutFailure(error.isEmpty() ? "Unable to logout. Please, try later."
							: error);
				}
			}

			@Override
			public void onFailure(int statusCode,
					org.apache.http.Header[] headers, byte[] responseBytes,
					java.lang.Throwable throwable) {
				Log.d("SUPER FAIL", "STATUS " + statusCode);
				if (handler != null) {
					handler.logoutFailure("Unable to logout. Please, try later.");
				}
			}
		});
	}

	public void register(final String username, final String password,
			final String email, final String mobile, final String firstName,
			final String lastName, final RegisterHandler handler) {
		RequestParams params = new RequestParams();
		params.put(USERNAME, username);
		params.put(PASSWORD, password);
		params.put(EMAIL, email);
		params.put(MOBILE, mobile);
		params.put(FIRST_NAME, firstName);
		params.put(LAST_NAME, lastName);

		Log.d("REGISTER PARAMS", params.toString());

		http.post(USER_URL, params, new JsonHttpResponseHandler() {
			@Override
			public void onSuccess(int statusCode, Header[] headers,
					JSONArray response) {
				Log.d("REGISTER RESPONSE", response.toString());
				if (handler != null) {
					String token = response.optJSONObject(1).optString(TOKEN);
					String id = response.optJSONObject(1).optString(ID);
					handler.registerSucceed(username, password, id, token,
							firstName, lastName, "");
				}
			}

			@Override
			public void onFailure(int statusCode,
					org.apache.http.Header[] headers,
					java.lang.Throwable throwable,
					org.json.JSONArray errorResponse) {
				if (handler != null) {
					String error = errorResponse.optString(1);
					handler.registerFailed(error.isEmpty() ? "Unable to register. Please, try later."
							: error);
				}
			}

			@Override
			public void onFailure(int statusCode,
					org.apache.http.Header[] headers, byte[] responseBytes,
					java.lang.Throwable throwable) {
				Log.d("SUPER FAIL", "STATUS " + statusCode + " " + new String(responseBytes));
				if (handler != null) {
					handler.registerFailed("Unable to register. Please, try later.");
				}
			}
		});
	}

	public void addFriend( final String token, final String id,
			final NewFriendsHandler handler ) {

		RequestParams params = new RequestParams();
		params.put(TOKEN, token);
		params.put(FRIEND_ID, id);

		Log.d("ADD FIENDS REQUEST", params.toString());

		http.post(FRIEND_URL, params, new JsonHttpResponseHandler() {

			@Override
			public void onSuccess(int statusCode, Header[] headers,
					JSONArray response) {

				Log.d("ADD FRIEND RESPONSE", response.toString());
				if (handler != null) {
					handler.addNewFriendsSucceed();
				}
			}

			@Override
			public void onFailure(int statusCode,
					org.apache.http.Header[] headers,
					java.lang.Throwable throwable,
					org.json.JSONArray errorResponse) {
				if (handler != null) {
					if (statusCode == 401) {
						APITalker.sharedTalker().login(
								User.sharedUser((Activity) handler).username(),
								User.sharedUser((Activity) handler).password(),
								new LoginHandler() {

									@Override
									public void loginSucceed(String username,
											String password, String userId,
											String newToken, String firstName,
											String lastName,
											String profileImageUri) {
										User.sharedUser((Activity) handler)
												.login(username, password,
														userId, newToken,
														firstName, lastName,
														profileImageUri);
										APITalker.sharedTalker().addFriend(
												newToken, id, handler);
									}

									@Override
									public void loginFailed(String error) {
										handler.addNewFriendsFailed("Unable to send friend request. Please, try again later");
									}
								});

						return;
					}

					String error = errorResponse.optString(1);
					handler.addNewFriendsFailed(error.isEmpty() ? "Unable to send friend request. Please, try again later"
							: error);
				}
			}

			@Override
			public void onFailure(int statusCode,
					org.apache.http.Header[] headers, byte[] responseBytes,
					java.lang.Throwable throwable) {
				Log.d("SUPER FAIL", "STATUS " + statusCode);
				if (handler != null) {
					if (statusCode == 401) {
						APITalker.sharedTalker().login(
								User.sharedUser((Activity) handler).username(),
								User.sharedUser((Activity) handler).password(),
								new LoginHandler() {

									@Override
									public void loginSucceed(String username,
											String password, String userId,
											String newToken, String firstName,
											String lastName,
											String profileImageUri) {
										User.sharedUser((Activity) handler)
												.login(username, password,
														userId, newToken,
														firstName, lastName,
														profileImageUri);
										APITalker.sharedTalker().addFriend(
												newToken, id, handler);
									}

									@Override
									public void loginFailed(String error) {
										handler.addNewFriendsFailed("Unable to send friend request. Please, try again later");
									}
								});

						return;
					}

					handler.addNewFriendsFailed("Unable to send friend request. Please, try again later");
				}
			}
		});
	}

	public void addComment(final String token, final String url,
			final double longitude, final double lattitude,
			final String comment, final String[] returnedFriendsIndexesArray,
			final String tags, final CommentHandler handler) {

		RequestParams params = new RequestParams();
		params.put(TOKEN, token);
		params.put(URL, url);
		params.put(LONG, longitude + "");
		params.put(LAT, lattitude + "");
		params.put(DESCRIPTION, comment);
		params.put(FRIENDS_IDS, returnedFriendsIndexesArray);
		params.put(TAGS, tags);

		Log.d("ADD COMMENT REQUEST", params.toString());


		http.post(COMMENT_URL, params, new JsonHttpResponseHandler() {

			@Override

			public void onSuccess(int statusCode, Header[] headers,
					JSONArray response) {
				Log.d("ADD COMMENT RESPONSE", response.toString());
				if (handler != null) {
					handler.commentSucceed(comment, response.optJSONObject(1)
							.optString(ID));
				}
			}

			@Override
			public void onFailure(int statusCode,
					org.apache.http.Header[] headers,
					java.lang.Throwable throwable,
					org.json.JSONArray errorResponse) {
				if (handler != null) {
					if (statusCode == 401) {
						APITalker.sharedTalker().login(
								User.sharedUser((Activity) handler).username(),
								User.sharedUser((Activity) handler).password(),
								new LoginHandler() {

									@Override
									public void loginSucceed(String username,
											String password, String userId,
											String newToken, String firstName,
											String lastName,
											String profileImageUri) {
										User.sharedUser((Activity) handler)
												.login(username, password,
														userId, newToken,
														firstName, lastName,
														profileImageUri);
										APITalker.sharedTalker().addComment(
												newToken, url, longitude,
												lattitude, comment,
												returnedFriendsIndexesArray,
												tags, handler);
									}

									@Override
									public void loginFailed(String error) {
										handler.commentFailure("Unable to send comment. Please, try again later");
									}
								});
						return;
					}

					String error = errorResponse.optString(1);
					handler.commentFailure(error.isEmpty() ? "Unable to send comment. Please, try again later"
							: error);
				}
			}

			@Override
			public void onFailure(int statusCode,
					org.apache.http.Header[] headers, byte[] responseBytes,
					java.lang.Throwable throwable) {
				Log.d("SUPER FAIL", "STATUS " + statusCode);
				if (handler != null) {
					if (statusCode == 401) {
						APITalker.sharedTalker().login(
								User.sharedUser((Activity) handler).username(),
								User.sharedUser((Activity) handler).password(),
								new LoginHandler() {

									@Override
									public void loginSucceed(String username,
											String password, String userId,
											String newToken, String firstName,
											String lastName,
											String profileImageUri) {
										User.sharedUser((Activity) handler)
												.login(username, password,
														userId, newToken,
														firstName, lastName,
														profileImageUri);
										APITalker.sharedTalker().addComment(
												newToken, url, longitude,
												lattitude, comment,
												returnedFriendsIndexesArray,
												tags, handler);
									}

									@Override
									public void loginFailed(String error) {
										handler.commentFailure("Unable to send comment. Please, try again later");
									}
								});
						return;
					}

					handler.commentFailure("Unable to send comment. Please, try again later");
				}
			}
		});
	}

	public void getComments(String token, final String url, int limit,
			final int skip, final FeedHandler handler) {
		RequestParams params = new RequestParams();
		params.put(TOKEN, token);
		params.put(LIMIT, LIMIT_DEFAULT + "");
		params.put(SKIP, skip + "");

		if (url != null) {
			Log.d("URL IS", url);
			params.put("url", url);
		}

		Log.d("GET COMMENT REQUEST", params.toString());
		http.get(COMMENT_URL, params, new JsonHttpResponseHandler() {

			@Override

			public void onSuccess(int statusCode, Header[] headers,
					JSONArray response) {
				Log.d("GET COMMENT RESPONSE", response.toString());
				if (handler != null) {
					JSONArray commentsArray = response.optJSONArray(1);
					ArrayList<Comment> comments = new ArrayList<Comment>();
					ArrayList<String> profileImagesUrls = new ArrayList<String>();
					SimpleDateFormat format = new SimpleDateFormat(
							"yyyy-MM-dd HH:mm:ss");

					for (int index = 0; index < commentsArray.length(); index++) {
						JSONObject commentJson = commentsArray
								.optJSONObject(index);
						Log.d("commentJson", commentJson.toString());
						JSONObject friendJson = commentJson.optJSONObject(USER);
						JSONObject createdJson = commentJson
								.optJSONObject("created");

						Friend friend = null;
						Date created = null;

						if (friendJson != null) {
							String profileImageUrl = BASE_URL + getCleanedUri(friendJson);
							friend = new Friend(friendJson.optString(USERNAME),
									friendJson.optString(ID), friendJson
											.optString(FIRST_NAME), friendJson
											.optString(LAST_NAME),
											profileImageUrl);
							// Populate a list of profile pictures urls
//							profileImagesUrls.add(profileImageUrl);
						}
						if (createdJson != null) {
							Log.d("HERE I AM", createdJson.toString());
							try {
								created = format.parse(createdJson
										.optString("date"));
							} catch (ParseException e) {
								e.printStackTrace();
							}
						}
						int commentLikesCount = getLikesCount(commentJson, COMMENT_LIKES);
						Log.d("LIKES COUNT", commentLikesCount + "");
						comments.add(new Comment(commentJson.optString(ID),
								commentJson.optString(DESCRIPTION), commentJson
										.optString(URL), friend, commentJson
										.optString(TAGS), commentLikesCount, created));
					}
					
					handler.feedCommentsSuccess(comments, profileImagesUrls);
				}
			}

			@Override
			public void onFailure(int statusCode,
					org.apache.http.Header[] headers,
					java.lang.Throwable throwable,
					org.json.JSONArray errorResponse) {
				if (handler != null) {
					if (statusCode == 401) {
						APITalker.sharedTalker().login(
								User.sharedUser((Activity) handler).username(),
								User.sharedUser((Activity) handler).password(),
								new LoginHandler() {

									@Override
									public void loginSucceed(String username,
											String password, String userId,
											String newToken, String firstName,
											String lastName,
											String profileImageUri) {
										User.sharedUser((Activity) handler)
												.login(username, password,
														userId, newToken,
														firstName, lastName,
														profileImageUri);
										APITalker.sharedTalker().getComments(
												newToken, url, LIMIT_DEFAULT,
												skip, handler);
									}

									@Override
									public void loginFailed(String error) {
										handler.feedCommentsFailure("Unable to get comments. Please, try again later");
									}
								});
						return;
					}

					String error = errorResponse.optString(1);
					handler.feedCommentsFailure(error.isEmpty() ? "Unable to get comments. Please, try again later"
							: error);
				}
			}

			@Override
			public void onFailure(int statusCode,
					org.apache.http.Header[] headers, byte[] responseBytes,
					java.lang.Throwable throwable) {
				Log.d("SUPER FAIL", "STATUS " + statusCode);
				if (handler != null) {
					if (statusCode == 401) {
						APITalker.sharedTalker().login(
								User.sharedUser((Activity) handler).username(),
								User.sharedUser((Activity) handler).password(),
								new LoginHandler() {

									@Override
									public void loginSucceed(String username,
											String password, String userId,
											String newToken, String firstName,
											String lastName,
											String profileImageUri) {
										User.sharedUser((Activity) handler)
												.login(username, password,
														userId, newToken,
														firstName, lastName,
														profileImageUri);
										APITalker.sharedTalker().getComments(
												newToken, url, LIMIT_DEFAULT,
												skip, handler);
									}

									@Override
									public void loginFailed(String error) {
										handler.feedCommentsFailure("Unable to get comments. Please, try again later");
									}
								});
						return;
					}

					handler.feedCommentsFailure("Unable to get comments. Please, try again later");
				}
			}
		});
	}

	private static int idGenerator = 0;

	public int getCommentsForSearch(String token, int limit, final int skip,
			final String word, final String tag, final FeedSearchHandler handler) {
		RequestParams params = new RequestParams();
		params.put(TOKEN, token);
		params.put(LIMIT, LIMIT_DEFAULT + "");
		params.put(SKIP, skip + "");
		params.put(WORD, word);
		params.put(TAG, tag);

		final int requestId = idGenerator + 1;

		Log.d("GET COMMENT SEARCH REQUEST", params.toString());
		http.get(COMMENT_SEARCH_URL, params, new JsonHttpResponseHandler() {

			@Override

			public void onSuccess(int statusCode, Header[] headers,
					JSONArray response) {
				Log.d("GET COMMENT SEARCH RESPONSE", response.toString());
				if (handler != null) {
					JSONArray commentsArray = response.optJSONArray(1);
					ArrayList<Comment> comments = new ArrayList<Comment>();
					ArrayList<String> profileImagesUrls = new ArrayList<String>();

					SimpleDateFormat format = new SimpleDateFormat(
							"yyyy-MM-dd HH:mm:ss");

					for (int index = 0; index < commentsArray.length(); index++) {
						JSONObject commentJson = commentsArray
								.optJSONObject(index);
						JSONObject friendJson = commentJson.optJSONObject(USER);
						JSONObject createdJson = commentJson
								.optJSONObject("created");
						Friend friend = null;
						Date created = null;

						if (friendJson != null) {
							String profileImageUrl = BASE_URL + getCleanedUri(friendJson);
							friend = new Friend(friendJson.optString(USERNAME),
									friendJson.optString(ID), friendJson
											.optString(FIRST_NAME), friendJson
											.optString(LAST_NAME),
											profileImageUrl);
//							profileImagesUrls.add(profileImageUrl);
						}
						if (createdJson != null) {
							try {
								created = format.parse(createdJson
										.optString("date"));
							} catch (ParseException e) {
								e.printStackTrace();
							}
						}
						int commentLikesCount = getLikesCount(commentJson, COMMENT_LIKES);
						comments.add(new Comment(commentJson.optString(ID),
								commentJson.optString(DESCRIPTION), commentJson
										.optString(URL), friend, commentJson
										.optString(TAGS), commentLikesCount, created));
					}

					handler.feedCommentsSuccess(comments, profileImagesUrls, requestId);
				}
			}

			@Override
			public void onFailure(int statusCode,
					org.apache.http.Header[] headers,
					java.lang.Throwable throwable,
					org.json.JSONArray errorResponse) {
				if (handler != null) {
					if (statusCode == 401) {
						APITalker.sharedTalker().login(
								User.sharedUser((Activity) handler).username(),
								User.sharedUser((Activity) handler).password(),
								new LoginHandler() {

									@Override
									public void loginSucceed(String username,
											String password, String userId,
											String newToken, String firstName,
											String lastName,
											String profileImageUri) {
										User.sharedUser((Activity) handler)
												.login(username, password,
														userId, newToken,
														firstName, lastName,
														profileImageUri);
										APITalker.sharedTalker()
												.getCommentsForSearch(newToken,
														LIMIT_DEFAULT, skip,
														word, tag, handler);
									}

									@Override
									public void loginFailed(String error) {
										handler.feedCommentsFailure(
												"Unable to get comments. Please, try again later",
												requestId);
									}
								});
						return;
					}

					String error = errorResponse.optString(1);
					handler.feedCommentsFailure(
							error.isEmpty() ? "Unable to get comments. Please, try again later"
									: error, requestId);
				}
			}

			@Override
			public void onFailure(int statusCode,
					org.apache.http.Header[] headers, byte[] responseBytes,
					java.lang.Throwable throwable) {
				Log.d("SUPER FAIL", "STATUS " + statusCode);
				if (handler != null) {
					if (statusCode == 401) {
						APITalker.sharedTalker().login(
								User.sharedUser((Activity) handler).username(),
								User.sharedUser((Activity) handler).password(),
								new LoginHandler() {

									@Override
									public void loginSucceed(String username,
											String password, String userId,
											String newToken, String firstName,
											String lastName,
											String profileImageUri) {
										User.sharedUser((Activity) handler)
												.login(username, password,
														userId, newToken,
														firstName, lastName,
														profileImageUri);
										APITalker.sharedTalker()
												.getCommentsForSearch(newToken,
														LIMIT_DEFAULT, skip,
														word, tag, handler);
									}

									@Override
									public void loginFailed(String error) {
										handler.feedCommentsFailure(
												"Unable to get comments. Please, try again later",
												requestId);
									}
								});
						return;
					}

					handler.feedCommentsFailure(
							"Unable to get comments. Please, try again later",
							requestId);
				}
			}
		});

		return requestId;
	}

	public void getCommentsByGeo(String token, int limit, final int skip,
			final double longitude, final double lattitude,
			final FeedHandler handler) {
		RequestParams params = new RequestParams();
		params.put(TOKEN, token);
		params.put(LIMIT, LIMIT_DEFAULT + "");
		params.put(SKIP, skip + "");
		params.put(LONG, longitude + "");
		params.put(LAT, lattitude + "");

		Log.d("GET COMMENT GEO REQUEST", params.toString());

		http.get(COMMENT_BY_GEO_URL, params, new JsonHttpResponseHandler() {

			@Override

			public void onSuccess(int statusCode, Header[] headers,
					JSONArray response) {
				Log.d("GET COMMENT GEO RESPONSE", response.toString());
				if (handler != null) {
					JSONArray commentsArray = response.optJSONArray(1);
					ArrayList<Comment> comments = new ArrayList<Comment>();
					ArrayList<String> profileImagesUrls = new ArrayList<String>();

					SimpleDateFormat format = new SimpleDateFormat(
							"yyyy-MM-dd HH:mm:ss");

					for (int index = 0; index < commentsArray.length(); index++) {
						JSONObject commentJson = commentsArray
								.optJSONObject(index);
						JSONObject friendJson = commentJson.optJSONObject(USER);
						JSONObject createdJson = commentJson
								.optJSONObject("created");
						Friend friend = null;
						Date created = null;

						if (friendJson != null) {
							String profileImageUrl = BASE_URL + getCleanedUri(friendJson);
							friend = new Friend(friendJson.optString(USERNAME),
									friendJson.optString(ID), friendJson
											.optString(FIRST_NAME), friendJson
											.optString(LAST_NAME),
											profileImageUrl);
//							profileImagesUrls.add(profileImageUrl);
						}
						if (createdJson != null) {
							try {
								created = format.parse(createdJson
										.optString("date"));
							} catch (ParseException e) {
								e.printStackTrace();
							}
						}
						int commentLikesCount = getLikesCount( commentJson, COMMENT_LIKES );
						comments.add(new Comment(commentJson.optString(ID),
								commentJson.optString(DESCRIPTION), commentJson
										.optString(URL), friend, commentJson
										.optString(TAGS), commentLikesCount, created));
					}

					handler.feedCommentsSuccess(comments, profileImagesUrls);
				}
			}

			@Override
			public void onFailure(int statusCode,
					org.apache.http.Header[] headers,
					java.lang.Throwable throwable,
					org.json.JSONArray errorResponse) {
				if (handler != null) {
					if (statusCode == 401) {
						APITalker.sharedTalker().login(
								User.sharedUser((Activity) handler).username(),
								User.sharedUser((Activity) handler).password(),
								new LoginHandler() {

									@Override
									public void loginSucceed(String username,
											String password, String userId,
											String newToken, String firstName,
											String lastName,
											String profileImageUri) {
										User.sharedUser((Activity) handler)
												.login(username, password,
														userId, newToken,
														firstName, lastName,
														profileImageUri);
										APITalker.sharedTalker()
												.getCommentsByGeo(newToken,
														LIMIT_DEFAULT, skip,
														longitude, lattitude,
														handler);
									}

									@Override
									public void loginFailed(String error) {
										handler.feedCommentsFailure("Unable to get comments. Please, try again later");
									}
								});
						return;
					}

					String error = errorResponse.optString(1);
					handler.feedCommentsFailure(error.isEmpty() ? "Unable to get comments. Please, try again later"
							: error);
				}
			}

			@Override
			public void onFailure(int statusCode,
					org.apache.http.Header[] headers, byte[] responseBytes,
					java.lang.Throwable throwable) {
				Log.d("SUPER FAIL", "STATUS " + statusCode);
				if (handler != null) {
					if (statusCode == 401) {
						APITalker.sharedTalker().login(
								User.sharedUser((Activity) handler).username(),
								User.sharedUser((Activity) handler).password(),
								new LoginHandler() {

									@Override
									public void loginSucceed(String username,
											String password, String userId,
											String newToken, String firstName,
											String lastName,
											String profileImageUri) {
										User.sharedUser((Activity) handler)
												.login(username, password,
														userId, newToken,
														firstName, lastName,
														profileImageUri);
										APITalker.sharedTalker()
												.getCommentsByGeo(newToken,
														LIMIT_DEFAULT, skip,
														longitude, lattitude,
														handler);
									}

									@Override
									public void loginFailed(String error) {
										handler.feedCommentsFailure("Unable to get comments. Please, try again later");
									}
								});
						return;
					}

					handler.feedCommentsFailure("Unable to get comments. Please, try again later");
				}
			}
		});
	}

	
	public void getFriends(String userId, final boolean isFollowersOnly,
			int limit, final int skip, final int status,
			final FriendsHandler handler) {
		RequestParams params = new RequestParams();
		params.put(LIMIT, LIMIT_DEFAULT + "");
		params.put(SKIP, skip + "");
		params.put(STATUS, status + "");

		Log.d("GET FRIENDS REQUEST", params.toString());

		Log.d("MFRIENDS", userId);
		String url = isFollowersOnly ? FOLLOWRS_URL : FRIEND_URL;
		Log.d("FOR URL", url);
		http.get(url + "/" + userId, params, new JsonHttpResponseHandler() {
			@Override
			public void onSuccess(int statusCode, Header[] headers,
					JSONArray response) {
				Log.d("GET FRIENDS RESPONSE", response.toString());
				if (handler != null) {
					JSONArray users = response.optJSONArray(1);
					ArrayList<Friend> friends = new ArrayList<Friend>();

					for (int index = 0; index < users.length(); index++) {
						JSONObject user = users.optJSONObject(index);
						friends.add(new Friend(user.optString(USERNAME), user
								.optString(ID), user.optString(FIRST_NAME),
								user.optString(LAST_NAME), null));
					}

					handler.getFriendsSuccess(friends);
				}
			}

			@Override
			public void onFailure(int statusCode,
					org.apache.http.Header[] headers,
					java.lang.Throwable throwable,
					org.json.JSONArray errorResponse) {
				Log.d("GET FRIENDS ERROR",
						statusCode + " " + errorResponse.toString());
				if (handler != null) {
					if (statusCode == 401) {
						APITalker.sharedTalker().login(
								User.sharedUser((Activity) handler).username(),
								User.sharedUser((Activity) handler).password(),
								new LoginHandler() {

									@Override
									public void loginSucceed(String username,
											String password, String userId,
											String newToken, String firstName,
											String lastName,
											String profileImageUri) {
										User.sharedUser((Activity) handler)
												.login(username, password,
														userId, newToken,
														firstName, lastName,
														profileImageUri);
										APITalker.sharedTalker().getFriends(
												userId, isFollowersOnly,
												LIMIT_DEFAULT, skip, status,
												handler);
									}

									@Override
									public void loginFailed(String error) {
										handler.getFriendsFailure("Unable to get friends. Please, try again later");
									}
								});
						return;
					}

					String error = errorResponse.optString(1);
					handler.getFriendsFailure(error.isEmpty() ? "Unable to get friends. Please, try again later"
							: error);
				}
			}

			@Override
			public void onFailure(int statusCode,
					org.apache.http.Header[] headers, byte[] responseBytes,
					java.lang.Throwable throwable) {
				Log.d("SUPER FAIL", "STATUS " + statusCode);
				if (handler != null) {
					if (statusCode == 401) {
						APITalker.sharedTalker().login(
								User.sharedUser((Activity) handler).username(),
								User.sharedUser((Activity) handler).password(),
								new LoginHandler() {

									@Override
									public void loginSucceed(String username,
											String password, String userId,
											String newToken, String firstName,
											String lastName,
											String profileImageUri) {
										User.sharedUser((Activity) handler)
												.login(username, password,
														userId, newToken,
														firstName, lastName,
														profileImageUri);
										APITalker.sharedTalker().getFriends(
												userId, isFollowersOnly,
												LIMIT_DEFAULT, skip, status,
												handler);
									}

									@Override
									public void loginFailed(String error) {
										handler.getFriendsFailure("Unable to get friends. Please, try again later");
									}
								});
						return;
					}

					handler.getFriendsFailure("Unable to get friends. Please, try again later");
				}
			}
		});
	}

	public int getUsers(int limit, final int skip, final UsersHandler handler) {
		RequestParams params = new RequestParams();
		params.put(LIMIT, LIMIT_DEFAULT + "");
		params.put(SKIP, skip + "");

		final int requestId = idGenerator++;

		http.get(USER_URL, params, new JsonHttpResponseHandler() {
			@Override
			public void onSuccess(int statusCode, Header[] headers,
					JSONArray response) {
				Log.d("GET USER RESPONSE", response.toString());
				if (handler != null) {
					JSONArray users = response.optJSONArray(1);
					ArrayList<Friend> friends = new ArrayList<Friend>();

					for (int index = 0; index < users.length(); index++) {
						JSONObject user = users.optJSONObject(index);
						friends.add(new Friend(user.optString(USERNAME), user
								.optString(ID), user.optString(FIRST_NAME),
								user.optString(LAST_NAME), null));
					}

					handler.getUsersSucceed(friends, requestId);
				}
			}

			@Override
			public void onFailure(int statusCode,
					org.apache.http.Header[] headers,
					java.lang.Throwable throwable,
					org.json.JSONArray errorResponse) {
				Log.d("GET FRIENDS ERROR",
						statusCode + " " + errorResponse.toString());
				if (handler != null) {
					if (statusCode == 401) {
						APITalker.sharedTalker().login(
								User.sharedUser((Activity) handler).username(),
								User.sharedUser((Activity) handler).password(),
								new LoginHandler() {

									@Override
									public void loginSucceed(String username,
											String password, String userId,
											String newToken, String firstName,
											String lastName,
											String profileImageUri) {
										User.sharedUser((Activity) handler)
												.login(username, password,
														userId, newToken,
														firstName, lastName,
														profileImageUri);
										APITalker.sharedTalker().getUsers(
												LIMIT_DEFAULT, skip, handler);
									}

									@Override
									public void loginFailed(String error) {
										handler.getUsersFailed(
												"Unable to get users. Please, try again later",
												requestId);
									}
								});
						return;
					}

					String error = errorResponse.optString(1);
					handler.getUsersFailed(
							error.isEmpty() ? "Unable to get users. Please, try again later"
									: error, requestId);
				}
			}

			@Override
			public void onFailure(int statusCode,
					org.apache.http.Header[] headers, byte[] responseBytes,
					java.lang.Throwable throwable) {
				Log.d("SUPER FAIL", "STATUS " + statusCode);
				if (handler != null) {
					if (statusCode == 401) {
						APITalker.sharedTalker().login(
								User.sharedUser((Activity) handler).username(),
								User.sharedUser((Activity) handler).password(),
								new LoginHandler() {

									@Override
									public void loginSucceed(String username,
											String password, String userId,
											String newToken, String firstName,
											String lastName,
											String profileImageUri) {
										User.sharedUser((Activity) handler)
												.login(username, password,
														userId, newToken,
														firstName, lastName,
														profileImageUri);
										APITalker.sharedTalker().getUsers(
												LIMIT_DEFAULT, skip, handler);
									}

									@Override
									public void loginFailed(String error) {
										handler.getUsersFailed(
												"Unable to get users. Please, try again later",
												requestId);
									}
								});
						return;
					}

					handler.getUsersFailed(
							"Unable to get users. Please, try again later",
							requestId);
				}
			}
		});

		return requestId;
	}

	public int searchUsers(int limit, final int skip, final String name,
			final UsersHandler handler) {
		RequestParams params = new RequestParams();
		params.put(LIMIT, LIMIT_DEFAULT + "");
		params.put(SKIP, skip + "");
		params.put("name", name);

		final int requestId = idGenerator++;

		http.get(USER_URL, params, new JsonHttpResponseHandler() {
			@Override
			public void onSuccess(int statusCode, Header[] headers,
					JSONArray response) {
				Log.d("GET USER RESPONSE", response.toString());
				if (handler != null) {
					JSONArray users = response.optJSONArray(1);
					ArrayList<Friend> friends = new ArrayList<Friend>();

					for (int index = 0; index < users.length(); index++) {
						JSONObject user = users.optJSONObject(index);
						friends.add(new Friend(user.optString(USERNAME), user
								.optString(ID), user.optString(FIRST_NAME),
								user.optString(LAST_NAME), null));
					}

					handler.getUsersSucceed(friends, requestId);
				}
			}

			@Override
			public void onFailure(int statusCode,
					org.apache.http.Header[] headers,
					java.lang.Throwable throwable,
					org.json.JSONArray errorResponse) {
				Log.d("GET FRIENDS ERROR",
						statusCode + " " + errorResponse.toString());
				if (handler != null) {
					if (statusCode == 401) {
						APITalker.sharedTalker().login(
								User.sharedUser((Activity) handler).username(),
								User.sharedUser((Activity) handler).password(),
								new LoginHandler() {

									@Override
									public void loginSucceed(String username,
											String password, String userId,
											String newToken, String firstName,
											String lastName,
											String profileImageUri) {
										User.sharedUser((Activity) handler)
												.login(username, password,
														userId, newToken,
														firstName, lastName,
														profileImageUri);
										APITalker.sharedTalker().searchUsers(
												LIMIT_DEFAULT, skip, name,
												handler);
									}

									@Override
									public void loginFailed(String error) {
										handler.getUsersFailed(
												"Unable to get users. Please, try again later",
												requestId);
									}
								});
						return;
					}

					String error = errorResponse.optString(1);
					handler.getUsersFailed(
							error.isEmpty() ? "Unable to get users. Please, try again later"
									: error, requestId);
				}
			}

			@Override
			public void onFailure(int statusCode,
					org.apache.http.Header[] headers, byte[] responseBytes,
					java.lang.Throwable throwable) {
				Log.d("SUPER FAIL", "STATUS " + statusCode);
				if (handler != null) {
					if (statusCode == 401) {
						APITalker.sharedTalker().login(
								User.sharedUser((Activity) handler).username(),
								User.sharedUser((Activity) handler).password(),
								new LoginHandler() {

									@Override
									public void loginSucceed(String username,
											String password, String userId,
											String newToken, String firstName,
											String lastName,
											String profileImageUri) {
										User.sharedUser((Activity) handler)
												.login(username, password,
														userId, newToken,
														firstName, lastName,
														profileImageUri);
										APITalker.sharedTalker().searchUsers(
												LIMIT_DEFAULT, skip, name,
												handler);
									}

									@Override
									public void loginFailed(String error) {
										handler.getUsersFailed(
												"Unable to get users. Please, try again later",
												requestId);
									}
								});
						return;
					}

					handler.getUsersFailed(
							"Unable to get users. Please, try again later",
							requestId);
				}
			}
		});

		return requestId;
	}

	public void getUserById(final String id, final UserHandler handler) {

		http.get(USER_URL + "/" + id, new JsonHttpResponseHandler() {
			@Override
			public void onSuccess(int statusCode, Header[] headers,
					JSONArray response) {
				Log.d("GET USERBYID RESPONSE", response.toString());
				if (handler != null) {
					JSONObject jsonObject = response.optJSONObject(1);
					String firstName = jsonObject.optString(FIRST_NAME);
					String lastName = jsonObject.optString(LAST_NAME);
					String userName = jsonObject.optString(USERNAME);
					String email = jsonObject.optString(EMAIL);
					String mobile = jsonObject.optString(MOBILE);
					String profileImageUri = getCleanedUri(response);
					handler.getUserSucceed(firstName, lastName, userName,
							email, mobile, profileImageUri);
				}
			}

			@Override
			public void onFailure(int statusCode,
					org.apache.http.Header[] headers,
					java.lang.Throwable throwable,
					org.json.JSONArray errorResponse) {
				Log.d("GET USERBYID ERROR",
						statusCode + " " + errorResponse.toString());
				if (handler != null) {
					if (statusCode == 401) {
						APITalker.sharedTalker().login(
								User.sharedUser((Activity) handler).username(),
								User.sharedUser((Activity) handler).password(),
								new LoginHandler() {

									@Override
									public void loginSucceed(String username,
											String password, String userId,
											String newToken, String firstName,
											String lastName,
											String profileImageUri) {
										User.sharedUser((Activity) handler)
												.login(username, password,
														userId, newToken,
														firstName, lastName,
														profileImageUri);
										APITalker.sharedTalker().getUserById(
												id, handler);
									}

									@Override
									public void loginFailed(String error) {
										handler.getUserFailed("Unable to get user profile. Please, try again later");
									}
								});
						return;
					}

					String error = errorResponse.optString(1);
					handler.getUserFailed(error.isEmpty() ? "Unable to get user profile. Please, try again later"
							: error);
				}
			}

			@Override
			public void onFailure(int statusCode,
					org.apache.http.Header[] headers, byte[] responseBytes,
					java.lang.Throwable throwable) {
				Log.d("SUPER FAIL", "STATUS " + statusCode);
				if (handler != null) {
					if (statusCode == 401) {
						APITalker.sharedTalker().login(
								User.sharedUser((Activity) handler).username(),
								User.sharedUser((Activity) handler).password(),
								new LoginHandler() {

									@Override
									public void loginSucceed(String username,
											String password, String userId,
											String newToken, String firstName,
											String lastName,
											String profileImageUri) {
										User.sharedUser((Activity) handler)
												.login(username, password,
														userId, newToken,
														firstName, lastName,
														profileImageUri);
										APITalker.sharedTalker().getUserById(
												id, handler);
									}

									@Override
									public void loginFailed(String error) {
										handler.getUserFailed("Unable to get user profile. Please, try again later");
									}
								});
						return;
					}

					handler.getUserFailed("Unable to get user profile. Please, try again later");
				}
			}
		});
	}

	public void updateProfile(final String firstName, final String lastName,
			final String userName, final String email, final String mobile,
			final String id, final UserHandler handler) {

		RequestParams params = new RequestParams();
		params.put(USERNAME, userName);
		params.put(EMAIL, email);
		params.put(MOBILE, mobile);
		params.put(FIRST_NAME, firstName);
		params.put(LAST_NAME, lastName);

		Log.d("UPDATE PROFILE PARAMS", params.toString());

		http.put(USER_URL + "/" + id, params, new JsonHttpResponseHandler() {
			@Override
			public void onSuccess(int statusCode, Header[] headers,
					JSONArray response) {
				Log.d("UPDATE PROFILE RESPONSE", response.toString());
				if (handler != null) {
					JSONObject jsonObject = response.optJSONObject(1);
					String profileImageUri = jsonObject
							.optString(RECEIVED_PROFILE_IMAGE_URI);
					handler.getUserSucceed(firstName, lastName, userName,
							email, mobile, profileImageUri);
				}
			}

			@Override
			public void onFailure(int statusCode,
					org.apache.http.Header[] headers,
					java.lang.Throwable throwable,
					org.json.JSONArray errorResponse) {
				Log.d("UPDATE PROFILE ERROR",
						statusCode + " " + errorResponse.toString());
				if (handler != null) {
					if (statusCode == 401) {
						APITalker.sharedTalker().login(
								User.sharedUser((Activity) handler).username(),
								User.sharedUser((Activity) handler).password(),
								new LoginHandler() {

									@Override
									public void loginSucceed(String username,
											String password, String userId,
											String newToken, String firstName,
											String lastName,
											String profileImageUri) {
										User.sharedUser((Activity) handler)
												.login(username, password,
														userId, newToken,
														firstName, lastName,
														profileImageUri);
										APITalker.sharedTalker().getUserById(
												id, handler);
									}

									@Override
									public void loginFailed(String error) {
										handler.getUserFailed("Unable to update user profile. Please, try again later");
									}
								});
						return;
					}

					String error = errorResponse.optString(1);
					handler.getUserFailed(error.isEmpty() ? "Unable to update user profile. Please, try again later"
							: error);
				}
			}

			@Override
			public void onFailure(int statusCode,
					org.apache.http.Header[] headers, byte[] responseBytes,
					java.lang.Throwable throwable) {
				Log.d("SUPER FAIL", "STATUS " + statusCode);

				if (handler != null) {
					if (statusCode == 401) {
						APITalker.sharedTalker().login(
								User.sharedUser((Activity) handler).username(),
								User.sharedUser((Activity) handler).password(),
								new LoginHandler() {

									@Override
									public void loginSucceed(String username,
											String password, String userId,
											String newToken, String firstName,
											String lastName,
											String profileImageUri) {
										User.sharedUser((Activity) handler)
												.login(username, password,
														userId, newToken,
														firstName, lastName,
														profileImageUri);
										APITalker.sharedTalker().getUserById(
												id, handler);
									}

									@Override
									public void loginFailed(String error) {
										handler.getUserFailed("Unable to update user profile. Please, try again later");
									}
								});
						return;
					}

					handler.getUserFailed("Unable to update user profile. Please, try again later");
				}
			}
		});
	}

	public void uploadImage(final String token, final String profileImageUri,
			final UploadImageHandler handler) {

		RequestParams params = new RequestParams();
		File myFile = new File(profileImageUri);
		try {
			params.put(PROFILE_IMAGE, myFile);
		} catch (FileNotFoundException e) {
		}

		params.put(TOKEN, token);

		Log.d("UPLOAD IMAGE REQUEST", params.toString());

		http.post(UPLOAD_IMAGE_URL, params, new JsonHttpResponseHandler() {

			@Override
			public void onSuccess(int statusCode, Header[] headers,
					JSONArray response) {

				Log.d("UPLOAD IMAGE RESPONSE", response.toString());
				if (handler != null) {
					handler.uploadImageSucceed(getCleanedUri(response));
				}
			}

			@Override
			public void onFailure(int statusCode,
					org.apache.http.Header[] headers,
					java.lang.Throwable throwable,
					org.json.JSONArray errorResponse) {
				if (handler != null) {
					if (statusCode == 401) {
						APITalker.sharedTalker().login(
								User.sharedUser((Activity) handler).username(),
								User.sharedUser((Activity) handler).password(),
								new LoginHandler() {

									@Override
									public void loginSucceed(String username,
											String password, String userId,
											String newToken, String firstName,
											String lastName,
											String profileImageUri) {
										User.sharedUser((Activity) handler)
												.login(username, password,
														userId, newToken,
														firstName, lastName,
														profileImageUri);
										APITalker.sharedTalker().uploadImage(
												newToken, profileImageUri,
												handler);
									}

									@Override
									public void loginFailed(String error) {
										handler.uploadImageFailed("Unable to upload image. Please, try again later");
									}
								});

						return;
					}

					String error = errorResponse.optString(1);
					handler.uploadImageFailed(error.isEmpty() ? "Unable to upload image. Please, try again later"
							: error);
				}
			}

			@Override
			public void onFailure(int statusCode,
					org.apache.http.Header[] headers, byte[] responseBytes,
					java.lang.Throwable throwable) {
				Log.d("SUPER FAIL", "STATUS " + statusCode);
				if (handler != null) {
					if (statusCode == 401) {
						APITalker.sharedTalker().login(
								User.sharedUser((Activity) handler).username(),
								User.sharedUser((Activity) handler).password(),
								new LoginHandler() {

									@Override
									public void loginSucceed(String username,
											String password, String userId,
											String newToken, String firstName,
											String lastName,
											String profileImageUri) {
										User.sharedUser((Activity) handler)
												.login(username, password,
														userId, newToken,
														firstName, lastName,
														profileImageUri);
										APITalker.sharedTalker().uploadImage(
												newToken, profileImageUri,
												handler);
									}

									@Override
									public void loginFailed(String error) {
										handler.uploadImageFailed("Unable to send friend request. Please, try again later");
									}
								});

						return;
					}

					handler.uploadImageFailed("Unable to send friend request. Please, try again later");
				}
			}
		});
	}

	public void getImage(final Context context, final String token,
			final String profileImageUri, final GetImageHandler handler) {
		RequestParams params = new RequestParams();
		params.put(TOKEN, token);
		Log.d("GET IMAGE REQUEST", params.toString());
		String IMAGE_URL = BASE_URL + profileImageUri;
		Log.d("MY IMAGE URL", IMAGE_URL);

		http.get(IMAGE_URL, new FileAsyncHttpResponseHandler(context) {

			@Override
			public void onSuccess(int statusCode, Header[] headers,
					File response) {
				Log.d("GET IMAGE RESPONSE", response.toString());
				if (handler != null) {
					String filePath = response.getPath();
					Log.d("FILE PATH", filePath);

					handler.getImageSucceed(filePath);
				}
			}

			@Override
			public void onFailure(int statusCode,
					org.apache.http.Header[] headers,
					java.lang.Throwable throwable, File file) {
				if (handler != null) {
					if (statusCode == 401) {
						APITalker.sharedTalker().login(
								User.sharedUser((Activity) handler).username(),
								User.sharedUser((Activity) handler).password(),
								new LoginHandler() {

									@Override
									public void loginSucceed(String username,
											String password, String userId,
											String newToken, String firstName,
											String lastName,
											String profileImageUri) {
										User.sharedUser((Activity) handler)
												.login(username, password,
														userId, newToken,
														firstName, lastName,
														profileImageUri);
										APITalker.sharedTalker().getImage(
												context, newToken,
												profileImageUri, handler);
									}

									@Override
									public void loginFailed(String error) {
										handler.getImageFailed("Unable to get image. Please, try again later");
									}
								});
						return;
					}

					String error = file.getName();
					handler.getImageFailed(error.isEmpty() ? "Unable to get image. Please, try again later"
							: error);
				}
			}

			@Override
			public void onFailure(int statusCode,
					org.apache.http.Header[] headers, byte[] responseBytes,
					java.lang.Throwable throwable) {
				Log.d("SUPER FAIL", "STATUS " + statusCode);
				if (handler != null) {
					if (statusCode == 401) {
						APITalker.sharedTalker().login(
								User.sharedUser((Activity) handler).username(),
								User.sharedUser((Activity) handler).password(),
								new LoginHandler() {

									@Override
									public void loginSucceed(String username,
											String password, String userId,
											String newToken, String firstName,
											String lastName,
											String profileImageUri) {
										User.sharedUser((Activity) handler)
												.login(username, password,
														userId, newToken,
														firstName, lastName,
														profileImageUri);
										APITalker.sharedTalker().getImage(
												context, newToken,
												profileImageUri, handler);
									}

									@Override
									public void loginFailed(String error) {
										handler.getImageFailed("Unable to get image. Please, try again later");
									}
								});
						return;
					}

					handler.getImageFailed("Unable to get image. Please, try again later");
				}
			}
		});
	}

	public void like( final String token, final String commentId, final LikeHandler handler ){
		RequestParams params = new RequestParams();
		params.put( TOKEN, token );
		params.put( COMMENT_ID, commentId );
		

		Log.d("LIKE REQUEST", params.toString());

		http.post( COMMENT_LIKE_URL, params, new JsonHttpResponseHandler() {
			
			@Override
			public void onSuccess(int statusCode, Header[] headers,
					JSONArray response) {
				Log.d("LIKE RESPONSE", response.toString());
				if (handler != null) {
					handler.likeSucceed( getLikesCount( response.optJSONObject(1), COMMENT_LIKES ) );
				}
			}

			@Override
			public void onFailure(int statusCode,
					org.apache.http.Header[] headers,
					java.lang.Throwable throwable,
					org.json.JSONArray errorResponse) {
				if (handler != null) {
					if (statusCode == 401) {
						APITalker.sharedTalker().login(
								User.sharedUser((Activity) handler).username(),
								User.sharedUser((Activity) handler).password(),
								new LoginHandler() {

									@Override
									public void loginSucceed(String username,
											String password, String userId,
											String newToken, String firstName,
											String lastName,
											String profileImageUri) {
										User.sharedUser((Activity) handler)
												.login(username, password,
														userId, newToken,
														firstName, lastName,
														profileImageUri);
										APITalker.sharedTalker().like(token, commentId, handler);
									}

									@Override
									public void loginFailed(String error) {
										handler.likeFailed("Unable to like. Please, try again later");
									}
								});
						return;
					}

					String error = errorResponse.optString(1);
					handler.likeFailed(error.isEmpty() ? "Unable to like. Please, try again later"
							: error);
				}
			}

			@Override
			public void onFailure(int statusCode,
					org.apache.http.Header[] headers, byte[] responseBytes,
					java.lang.Throwable throwable) {
				Log.d("SUPER FAIL", "STATUS " + statusCode);
				if (handler != null) {
					if (statusCode == 401) {
						APITalker.sharedTalker().login(
								User.sharedUser((Activity) handler).username(),
								User.sharedUser((Activity) handler).password(),
								new LoginHandler() {

									@Override
									public void loginSucceed(String username,
											String password, String userId,
											String newToken, String firstName,
											String lastName,
											String profileImageUri) {
										User.sharedUser((Activity) handler)
												.login(username, password,
														userId, newToken,
														firstName, lastName,
														profileImageUri);
										APITalker.sharedTalker().like(token, commentId, handler);
									}

									@Override
									public void loginFailed(String error) {
										handler.likeFailed("Unable to send comment. Please, try again later");
									}
								});
						return;
					}

					handler.likeFailed("Unable to like. Please, try again later");
				}
			}
		});
	}
	
	
	// Get the cleaned ( RELATIVE!!! ) uri of a profile image on the server
	public String getCleanedUri(Object response) {
		String receivedProfileImageUri;
		if (response.getClass() == JSONArray.class) {
			JSONArray responseArray = (JSONArray) response;
			receivedProfileImageUri = responseArray.optJSONObject(1).optString(
					RECEIVED_PROFILE_IMAGE_URI);
		} else {
			JSONObject responseObject = (JSONObject) response;
			receivedProfileImageUri = responseObject
					.optString(RECEIVED_PROFILE_IMAGE_URI);
		}

		Log.d("RECEIVED PROFILE IMAGE URI", receivedProfileImageUri);
		StringBuilder builder = new StringBuilder(receivedProfileImageUri);
		if( receivedProfileImageUri == "null" ){
			receivedProfileImageUri = "";
		}
		else if ( builder.toString() != "") {
			builder.deleteCharAt(0);
			receivedProfileImageUri = builder.toString();
		}
		return receivedProfileImageUri;

	}

	public int getLikesCount( JSONObject commentJson , String likesString ){

		String commentLike = commentJson.optString( likesString );
		int commentLikeCount = Integer.parseInt(commentLike);
		return commentLikeCount;
	}
}