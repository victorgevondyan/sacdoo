package com.sagdoo.talkers;

public interface CommentHandler {
	public void commentSucceed(String comment, String id );
	public void commentFailure(String error);
}