package com.sagdoo.talkers;

public interface UserHandler {
	public void getUserSucceed(String firstName, String lastName,
			String userName, String email, String mobile, String profileImageUri);

	public void getUserFailed(String error);
}
