package com.sagdoo;

import java.util.ArrayList;

import android.app.Activity;
import android.app.ProgressDialog;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.sagdoo.models.Friend;
import com.sagdoo.models.User;
import com.sagdoo.talkers.APITalker;
import com.sagdoo.talkers.FriendsHandler;

public class AddFriendsActivity extends Activity implements FriendsHandler {
	public static final String CHECKED_FRIENDS_LIST = "CHECKED_FRIENDS_LIST";

	ProgressDialog loadingDialog;
	ListView listView;
	AddFriendsAdapter adapter;
	ArrayList<Friend> checkedFriendsArray = new ArrayList<Friend>();
	
	private boolean lockRequests = false;
	private boolean lockForever = false;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_friends);

		loadingDialog = new ProgressDialog(this);
		loadingDialog.setCancelable(false);
		loadingDialog.setCanceledOnTouchOutside(false);
		loadingDialog.show();
		
		lockRequests = true;
		APITalker.sharedTalker().getFriends(
				User.sharedUser(this).userId(), false, 25, 0, 1,
				this);
		
		adapter = new AddFriendsAdapter(this, R.layout.select_friend_item, new ArrayList<Friend>());
		listView = (ListView) findViewById(R.id.freinds);
		listView.setAdapter(adapter);
		
		listView.setOnScrollListener(AddFriendsListScrollListener);
	}
	
	public OnScrollListener AddFriendsListScrollListener = new OnScrollListener() {

		@Override
		public void onScrollStateChanged(AbsListView view, int scrollState) {
			
		}

		@Override
		public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
			if ( firstVisibleItem >= totalItemCount - 10 && !lockRequests && !lockForever ) {
				APITalker talker = APITalker.sharedTalker();
				talker.getFriends(
						User.sharedUser(AddFriendsActivity.this).userId(), false, 25, adapter.getCount(), 1,
						AddFriendsActivity.this);
			}
		}
	};
	
	@Override
	public void onBackPressed() {
		Intent result = new Intent();
		result.putParcelableArrayListExtra(CHECKED_FRIENDS_LIST, checkedFriendsArray);
		setResult(Activity.RESULT_OK, result);
		finish();
		
		super.onBackPressed();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		return super.onOptionsItemSelected(item);
	}
	
	/*
	 * ListView Adapter
	 */
	
	private class AddFriendsAdapter extends ArrayAdapter<Friend> {
		private LayoutInflater inflater;
		private ArrayList<Friend> friends;
		private int resource;
		

		public AddFriendsAdapter(Context context, int resource, ArrayList<Friend> friends) {
			super(context, resource, friends);
			
			this.resource = resource;
			this.friends = friends;
			this.inflater = (LayoutInflater) context.getSystemService(Service.LAYOUT_INFLATER_SERVICE);
		}
		
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			if (convertView == null) {
				convertView = inflater.inflate(resource, parent, false);
			}
			
			Friend friend = getItem(position);
			TextView name = (TextView) convertView.findViewById(R.id.name);
			name.setText(friend.firstName() + " " + friend.lastName());
			
			CheckBox friendCheckBox = (CheckBox) convertView.findViewById(R.id.add_icon);
			
			friendCheckBox.setOnCheckedChangeListener(friendCheckBoxCheckedListener);
			
			return convertView;
		}
		
		public OnCheckedChangeListener  friendCheckBoxCheckedListener = new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				View parent = (View) buttonView.getParent();
				int index = listView.getPositionForView(parent);
				Friend friend = adapter.getItem(index);
				
				if( isChecked ){
					checkedFriendsArray.add(friend);
				}
				else{
					checkedFriendsArray.remove(friend);
				}
				
			}
		}; 
		
		@Override
		public Friend getItem(int position) {
			return friends.get(position);
		}
		
		public void addAtEnd(ArrayList<Friend> newFriends) {
			friends.addAll(newFriends);
			notifyDataSetChanged();
		}
	}
	
	/*
	 * FriendsHandler Methods
	 */

	@Override

	public void getFriendsSuccess(ArrayList<Friend> friends) {
		TextView noFriendsTextView = (TextView) findViewById(R.id.no_friends_add);
		
		if( friends.isEmpty() && adapter.getCount() == 0 ) {
			noFriendsTextView.setVisibility(View.VISIBLE);
		}
		
		if (friends.size() > 0) {
			adapter.addAtEnd(friends);
		}
		
		if (friends.size() < 25) {
			lockForever = true;
		}
		
		lockRequests = false;
		
		loadingDialog.dismiss();
	}

	@Override
	public void getFriendsFailure(String error) {
		loadingDialog.dismiss();
		
		Context context = getApplicationContext();
		CharSequence text = error;
		int duration = Toast.LENGTH_SHORT;

		Toast toast = Toast.makeText(context, text, duration);
		toast.show();
		
		lockRequests = false;
	}

	/*Add all and clear all users to friends list */
	
	public void addAll(View view){
		listView = (ListView) findViewById(R.id.freinds);
		int listLength = listView.getChildCount();
		View listViewItem;
		CheckBox listViewSubitem;
		for(int i = 0; i < listLength; i++){
			listViewItem = listView.getChildAt(i);
			listViewSubitem = (CheckBox)listViewItem.findViewById(R.id.add_icon);
			listViewSubitem.setChecked(true);
		}
	}
	
	public void clearAll(View view){
		listView = (ListView) findViewById(R.id.freinds);
		int listLength = listView.getChildCount();
		View listViewItem;
		CheckBox listViewSubitem;
		for(int i = 0; i < listLength; i++){
			listViewItem = listView.getChildAt(i);
			listViewSubitem = (CheckBox)listViewItem.findViewById(R.id.add_icon);
			listViewSubitem.setChecked(false);
		}
	}
}

