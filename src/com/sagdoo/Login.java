package com.sagdoo;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.Toast;

import com.sagdoo.models.User;
import com.sagdoo.talkers.APITalker;
import com.sagdoo.talkers.LoginHandler;

public class Login extends Activity implements LoginHandler {
	private EditText txtUsername;
	private EditText txtpassword;
	
	public static int ACTIVITY_RESULT = 9900;
	
	private User user;
	
	ProgressDialog loadingDialog;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		
		loadingDialog = new ProgressDialog(this);
		
		user = User.sharedUser(this);
		
		if (user.isLoggedIn()) {
			Intent intent = new Intent(Login.this, Feed.class);
			startActivityForResult(intent, Feed.ACTIVITY_RESULT);
			
			return;
		}
		
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if ( requestCode == Feed.ACTIVITY_RESULT) {
			if( resultCode == RESULT_OK ){ 
				return; 
			} else {
				finish();
			}
		}
		if (requestCode == Register.REQUEST_CODE) {
			Intent intent = new Intent(Login.this, Feed.class);
			startActivityForResult(intent, Feed.ACTIVITY_RESULT);
		}
	}
	
	public void signup(View v) {
		Intent intent = new Intent(this, Register.class);
		startActivityForResult(intent, Register.REQUEST_CODE);
	}
	
	public void login(View v) {
		txtUsername = (EditText) findViewById(R.id.username);
		txtpassword = (EditText) findViewById(R.id.password);
		
		final String uname = txtUsername.getText().toString();
		final String pass = txtpassword.getText().toString();
		
		if(uname.equals("") || pass.equals("")){
			Toast toast = Toast.makeText(getApplicationContext(), "Please Enter Username & Password", Toast.LENGTH_SHORT);
			toast.show();
		} else {
			loadingDialog.setCancelable(false);
			loadingDialog.setCanceledOnTouchOutside(false);
			loadingDialog.show();
			APITalker.sharedTalker().login(uname, pass, Login.this);
		}
	}
	
	/*
	 * LoginHandler Methods
	 */

	@Override
	public void loginSucceed(String username, String password, String userId, String token, String firstName, String lastName, String profileImageUri ) {
		User.sharedUser(this).login(username, password, userId, token, firstName, lastName, profileImageUri );
		
		Intent intent = new Intent(Login.this, Feed.class);
		startActivityForResult(intent, Feed.ACTIVITY_RESULT);
		
		loadingDialog.dismiss();
	}

	@Override
	public void loginFailed(String error) {
		Toast toast = Toast.makeText(getApplicationContext(), error, Toast.LENGTH_SHORT);
		toast.show();
		
		loadingDialog.dismiss();
	}
}