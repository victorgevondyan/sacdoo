package com.sagdoo;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;

public class BrowserWidgetProvider extends AppWidgetProvider {
	
	public void onUpdate(Context context, AppWidgetManager appWidgetManager,int[] appWidgetIds) {
		RemoteViews remoteViews = new RemoteViews(context.getPackageName (), R.layout.browser_widget);
		Intent launchActivity = new Intent(context, Browser.class);
		PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, launchActivity, 0);
		remoteViews.setOnClickPendingIntent(R.id.widget, pendingIntent);
		 
		ComponentName thisWidget = new ComponentName(context, BrowserWidgetProvider.class);
		AppWidgetManager manager = AppWidgetManager.getInstance(context);
		manager.updateAppWidget(thisWidget, remoteViews);
	}
}
